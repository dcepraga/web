<!DOCTYPE html>

<html>
	
	<head>
		<title>Pagina trimite form</title>
	</head>

	<body>
		
		<form method="post" action="" class="formular">
			<p>Nume:</p>
			<input type="text" name="nume" class="nume" placeholder="Introbagati-va numele, va rag!">
			<p>Prenume:</p>
			<input type="text" name="prenume" class="prenume" placeholder="Introbagati-va prenumele, va rag!">
			<p>Tzelefon:</p>
			<input type="text" name="telefon" class="telefon" placeholder="Introbagati-va nr. de tzelefon, va rag!">
			<p>Email:</p>
			<input type="text" name="email" class="email" placeholder="Introbagati-va email-ul, va rag!">
			<p>Departament:</p>
			<select class="selecteaza_dep" name="selecteaza_dep">
				<option value="departament info">Departament ageamii!</option>
				<option value="departament contabilitate">Departamentul Anticoruptie!</option>
				<option value="departament HR">Departament pizde proaste!</option>
			</select>
			<input type="checkbox" name="valoare1" value="valoare1" class="checkbox">Check
			<p>Comentarii:</p>
			<textarea class="mesaj" name="mesaj" placeholder="Introbagati-va mesajul, va rag!"></textarea>
			<input type="button" class="trimite_formular" value="Trimite Formular">
		</form>
		<div class="afiseaza_mesaj" style="font-size: 50px; text-align: center; color: green; margin-top: 50px; display: none;">
			Formularul a fost trimis cu success!
		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script type="text/javascript" src="/web/js/formularJS.js"></script>
	</body>

</html>