<!DOCTYPE html>

<html>
	
	<head>
		<meta charset="UTF-8">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="/web/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/web/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="/web/css/benedict.css">
		<link rel="stylesheet" type="text/css" href="/web/css/benedictresponsive.css">
		<title> Benedict </title>
	</head>

	<body>
		<div class="div_800">

			<div class="header1">
				<a target="blank" href="../../.." title="Browser view"><p class="text-center browser">View in Browser</p></a>
				<a target="blank" href="../../.." title="Call us"><p class="text-center contact">Contact Us: +98 04 555 67 89</p></a>
				<div class="clear"></div>
			</div>

			<div class="header2">
				<a target="blank" href="../../.." title="View in browser"><p class="text-center">View this email in your browser</p></a>
			</div>

			<div class="header3">
				<div class="benedict"></div>
				<div class="buttons">
					<a href="../../.." title="About Us">About Us</a>
					<a href="../../.." title="Products">Products</a>
					<a href="../../.." title="Sign In">Sign In</a>
				</div>
				<div class="clear"></div>
			</div>

			<div class="solutions">
				<div class="col-md-offset-4 col-md-8 div_maglavais">
					<span class="animation_solutions">Solutions</span>
					<p class="for_you animation_foryou">For You</p>
					<p class="opacity0 animation_text">Lorem ipsum dolor sit amet, consectetur adipiscing<br>elit, sed do eiusmod tempor incididunt ut labore et<br>dolore magna aliqua.</p>
					<div class="video animation_video">
						<p>View Video Tour</p>
						<div class="play"></div>
					</div>
				</div>
			</div>

			<div class="template">
				<div class="container_texts">
					<p class="text_template">Welcome to the<br>Benedict Template.<br>We are glad to see you.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor<br>incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud<br>exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute<br>irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla<br>pariatur. Exceptetur sint occaecat cupidatat non proident, sunt in culpa qui officia<br>deserunt mollit anim id est laborum.</p>
				</div>
			</div>

			<div class="company">
				<div class="col-md-5 div_company">
					<p class="about">About our<br>company</p>
					<p>Lorem ipsum dolor sit amet,<br>consectetur adipisicing elit,<br>sed od eiusmod tempor in<br>cididunt ut labore et dolore<br>magna aliqua. Ut enim ad<br>minim veniam, quis nostrud<br>exercitation ullamco laboris<br>nisi ut aliquip ex ea commo<br>do conse kolit androt amma<br>quat. Duis aute iru re dolor<br>in reprehen derit in leruka<br>voluptate velit.</p>
					<a target="blank" href="../../.." title="Read more">Read More <i class="fa fa-angle-right"></i></a>
				</div>

				<div class="col-md-7 sea" alt="Coastline picture"></div>
			</div>

			<div class="services">

				<p class="text-center our_services">Our Services</p>
				<div class="horizontal"></div>
				<p class="text-center">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea<br>commodo consequat</p>

				<div class="container_options">
					<div class="options margin_right">
						<img class="poza" src="/web/img/Benedict/ico-service1.png">
						<p class="thick">Clean design</p>
						<p>Ut enim ad minim veniam,<br>quis nostrud exercitation<br>ullamco laboris.</p>
					</div>

					<div class="options margin_right">
						<img class="poza" src="/web/img/Benedict/ico-service2.png">
						<p class="thick">Color editable</p>
						<p>Lorem ipsum dolor sit amet,<br>consectetur adipisicing elit,<br>sed do eiusmod tempor.</p>
					</div>

					<div class="options">
						<img class="poza" src="/web/img/Benedict/ico-service3.png">
						<p class="thick">Color editable</p>
						<p>Tempor incididunt ut labore<br>et dolore magna aliqua<br>nostrud exercitation.</p>
					</div>
					<div class="clear"></div>
				</div>

				<div class="read_more">
					<a target="blank" href="../../.." title="Read more">Read More</a>
				</div>

			</div>

			<div class="story">
				<div class="col-md-6 padding0">
					<div class="col-md-6 story_pic padding0">
						<img src="/web/img/Benedict/3.jpg">
					</div>

					<div class="col-md-6 story_pic padding0">
						<img src="/web/img/Benedict/4.jpg">
					</div>
				</div>

				<div class="col-md-6 text_height">
					<p class="text_story">Benedict story</p>
					<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip<br>ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit<br>esse cillum dolore eu fugiat nulla pariatur.</p>
					<a target="blank" href="../../.." title="Read more">Read More <i class="fa fa-angle-right"></i></a>
				</div>
				<div class="clear"></div>
			</div>

			<div class="location">
				<div class="col-md-6 padding_other">
					<p class="our_location">Our location</p>
					<p>Ut enim ad minim veniam, quis nostrud<br>exercitation ullamco laboris nisi ut aliquip<br>ex ea commodo consequat. Duis aute irure<br>dolor in reprehenderit in voluptate velit.</p>
					<div class="horizontal_curls"></div>
					<p class="best_choice">Best choice</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor<br>incididunt ut labore et dolore magna aliqu at enim ad minim.</p>
					<div class="read_more2">
						<a target="blank" href="../../.." title="Read more">Read More</a>
					</div>
				</div>
				<div class="col-md-6 pic_location">
					<img src="/web/img/Benedict/5.jpg">
				</div>
				<div class="clear"></div>
			</div>

			<div class="news">

				<p class="from_us">News from us</p>
				<p class="margin_left">Duis aute irure dolor in reprehenderit in velit esse cillum eu fugiat nulla pariatur.</p>

				<div class="col-md-8 margin_top">
					<p class="guitarist_Los_Angles">New guitarist in our group</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed<br>do eiusmod tempor incididunt ut labore et dolore magna<br>aliqua. Ut enim ad minim veniam, quis nostrud exercitation<br>ullamco laboris nisi ut aliquip ex ea commodo consequat.<br>Duis aute irure dolor in reprehenderit in voluptate velit esse<br>cillum dolore eu fugiat nulla pariatur. Exceptetur sint occaecat<br>cupidatat non proident, sunt in culpa qui officia deserunt<br>mollit anim id est laborum.</p>
					<a target="blank" href="../../.." title="Read more">Read More <i class="fa fa-angle-right"></i></a>
				</div>

				<div class="col-md-4 margin_top">
					<p class="guitarist_Los_Angles">Los Angeles</p>
					<p>Sed ut perspiciatis unde om<br>nis iste natus error sit volup<br>tatem accusantium dolorem<br>que laudantium, totam rem<br>aperiam, eaque ipsa quae ab<br>illo inventore veritatis et<br>quasi architecto beatae vitae<br>dicta sunt explicabo.</p>
					<a target="blank" href="../../.."  title="Read more">Read More <i class="fa fa-angle-right"></i></a>
				</div>
				<div class="clear"></div>
			</div>

			<div class="single_photo">
				<img src="/web/img/Benedict/6.jpg">
			</div>

			<div class="blog">
				<p class="text-center blog2">Blog</p>

				<div class="col-md-6 blog_pics1 padding0">
					<img src="/web/img/Benedict/7.jpg">
					<p class="text_blog">Lorem ipsum dolor sit<br>amet, consectetur</p>
					<p>Lorem ipsum dolor sit amet, consectetur<br>adipisicing elit, sed do eiusmod tempor in<br>cididunt ut labore et dolore magna aliqu at<br>enim ad minim.</p>
					<a target="blank" href="../../.."  title="Read more">Read More <i class="fa fa-angle-right"></i></a>
				</div>

				<div class="col-md-6 blog_pics2 padding0">
					<img src="/web/img/Benedict/8.jpg">
					<p class="text_blog">Sed do eiusmod tempor<br>incididunt ut labore</p>
					<p>Ut enim ad minim veniam, quis nostrud<br>exercitation ullamco laboris nisi ut aliquip<br>ex ea commodo consequat. Duis aute irure<br>dolor in reprehenderit in voluptate velit.</p>
					<a target="blank" href="../../.."  title="Read more">Read More <i class="fa fa-angle-right"></i></a>
				</div>
				<div class="clear"></div>
			</div>

			<div class="team">

				<p class="text-center text_team">Our team</p>
				<img src="/web/img/Benedict/title-line.png" align="Middle"></img>
				<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut<br>labore et dolore magna aliqua.</p>

				<div class="container_people">		
					<div class="people">
						<img src="/web/img/Benedict/9.jpg">
						<p class="text-center names">Mark<br>Strotman</p>
						<p class="text-center icons">
							<a target="blank" href="https://www.facebook.com/" title="Facebook.com"><i class="fa fa-facebook-f"></i></a>
							<a target="blank" href="https://twitter.com/" title="Twitter.com"><i class="fa fa-twitter"></i></a>
							<a target="blank" href="https://www.instagram.com" title="Instagram.com"><i class="fa fa-instagram"></i></a>
						</p>
					</div>

					<div class="people">
						<p class="text-center names">Carmen<br>Greendeval</p>
						<p class="text-center icons">
							<a target="blank" href="https://www.facebook.com/" title="Facebook.com"><i class="fa fa-facebook-f"></i></a>
							<a target="blank" href="https://twitter.com/" title="Twitter.com"><i class="fa fa-twitter"></i></a>
							<a target="blank" href="https://www.instagram.com" title="Instagram.com"><i class="fa fa-instagram"></i></a>
						</p>
						<img src="/web/img/Benedict/10.jpg">
					</div>

					<div class="people">
						<img src="/web/img/Benedict/11.jpg">
						<p class="text-center names">Steve<br>McConnan</p>
						<p class="text-center icons">
							<a target="blank" href="https://www.facebook.com/" title="Facebook.com"><i class="fa fa-facebook-f"></i></a>
							<a target="blank" href="https://twitter.com/" title="Twitter.com"><i class="fa fa-twitter"></i></a>
							<a target="blank" href="https://www.instagram.com" title="Instagram.com"><i class="fa fa-instagram"></i></a>
						</p>
					</div>

					<div class="people">
						<p class="text-center names">Nataly<br>Scott</p>
						<p class="text-center icons">
							<a target="blank" href="https://www.facebook.com/" title="Facebook.com"><i class="fa fa-facebook-f"></i></a>
							<a target="blank" href="https://twitter.com/" title="Twitter.com"><i class="fa fa-twitter"></i></a>
							<a target="blank" href="https://www.instagram.com" title="Instagram.com"><i class="fa fa-instagram"></i></a>
						</p>
						<img src="/web/img/Benedict/12.jpg">
					</div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="benefits_container">

				<div class="col-md-6 working_pic">
					<img src="/web/img/Benedict/13.jpg" alt="Working with laptop image." align="Middle" width="100%" height="605.017px">
				</div>

				<div class="col-md-6 benefits">
					<p class="our_benefits"><b>Our benefits</b></p>
					<p>Ullamco laboris nisi ut aliquip ex ea com<br>modo consequat. Duis aute irure dolor in<br>reprehenderit in voluptate velit esse cillum<br>dolore eu fugiat nulla pariatur exceptetur<br>sint occaecat cupidatat.</p>
					<div class="quality_pic">
						<p class="list"><b>Unique design</b></p>
						<p class="list"><b>Responsive template</b></p>
						<p class="list"><b>Customisable layout</b></p>
						<p class="list"><b>Quality builder</b></p>
						<p class="list"><b>Profesh support</b></p>
					</div>
					<a target="blank" href="../../.." title="Read more">Read More</a>
				</div>
				<div class="clear"></div>

			</div>

			<div class="watch">
				<p class="text-center watch_video"><b>Watch video</b></p>
				<p class="text-center"><font color="white">Totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae<br>vitae dicta sunt explicabo. Sed ut perspiciatis unde om nis iste natus error sit volup tatem<br>accusantium dolorem que laudantium.</font></p>
				<div class="play_pic"></div>
				<div class="clear"></div>
			</div>

			<div class="principles">
				<div class="col-md-7 coast_pic" alt="Coastline picture"></div>

				<div class="col-md-5 working_principles">
					<p class="our_principles"><b>Our working<br>principles</b></p>
					<p><font color="white">Ut enim ad minim veniam,<br>quis nostrud exercitation<br>ullamco laboris nisi ut aliqu<br>ip ex ea commo do conse<br>kolit androt amma quat.<br>Duis aute irure dolor in repr<br>ehenderit in leruka volup<br>tate velit. Lorem ipsum dolor<br>sit amet, consectetur adipisi<br>cing elit, sed do eiusmod<br>tempor incididunt ut labore<br>et dolore magna aliqua.</font></p>
					<a target="blank" href="../../.."  title="Read more">Read More <i class="fa fa-angle-right"></i></a>
				</div>
				<div class="clear"></div>
			</div>

			<div class="gallery">
				
				<span><b>Gallery</b></span>
				<img src="/web/img/Benedict/title-line.png" alt="Curls pic." align="Middle" width="100%" height="10px">

				<div class="container_pictures">
					<div class="col-md-4 container_left">
						<div class="col-md-12 margin_bottom padding0 rgba">
							<a href="../../.." title="View gallery"><div class="div_rgba"></div></a>
							<img src="/web/img/Benedict/15.jpg" alt="Coast picture.">
						</div>
						<div class="col-md-12 padding0 rgba">
							<a href="../../.." title="View gallery"><div class="div_rgba"></div></a>
							<img src="/web/img/Benedict/16.jpg" alt="Road by the coast picture.">
						</div>
					</div>

					<div class="col-md-4 container_middle rgba">
						<a href="../../.." title="View gallery"><div class="div_rgba"></div></a>
						<img src="/web/img/Benedict/17.jpg" alt="Dock by the coast picture.">
					</div>

					<div class="col-md-4 container_right">
						<div class="col-md-12 margin_bottom padding0 rgba">
							<a href="../../.." title="View gallery"><div class="div_rgba"></div></a>
							<img src="/web/img/Benedict/18.jpg" alt="Mountains picture.">
						</div>
						<div class="col-md-12 padding0 rgba">
							<a href="../../.." title="View gallery"><div class="div_rgba"></div></a>
							<img src="/web/img/benedict/19.jpg" alt="Bridge picture.">
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>

			<div class="wave">
				<i class="fa fa-quote-right"></i>
				<p class="guy_text"><font color="white" size="+1">Lorem ipsum dolor sit amet,<br>consectetur adipisicing elit, sed do<br>eiusmod tempor incididunt ut labore<br>et dolore magna aliqua. Ut enim ad<br>minim veniam, quis nostrud<br>exercitation ullamco laboris nisi ut<br>aliquipex ea commodo consequat.</font></p>
				<span><b>Thomas Rashfold</b></span>
				<div class="guy"></div>
			</div>

			<div class="contacts">
				<div class="col-md-5 container_contacts">
					<span><b>Our contacts</b></span>
					<address>76087 Charlotte Street,<br>London 6 W1T 1RJ, England</address>
					<p>+79 859 342 89</p>
					<p>+79 567 321 74</p>
					<address>office@benedict.com<br>www.benedict.com</address>
					<p>Follow us:</p>
					<i class="fa fa-instagram"></i>
					<i class="fa fa-facebook-f"></i>
					<i class="fa fa-twitter"></i>
					<i class="fa fa-linkedin"></i>
					<i class="fa fa-pinterest"></i>
				</div>
				<img src="/web/img/Benedict/21.jpg" alt="City picture." height="242.5px">
			</div>

			<div class="footer">
				<p>View in Browser</p>
				<p class="our_website">Go to our Website</p>
				<p class="unsubscribe">unsubscribe</p>
				<div class="clear"></div>
			</div>

		</div>

	</body>

</html>