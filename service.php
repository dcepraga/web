<!DOCTYPE html>

<html>

	<body>

		<?php
			include('/elements/head.php');
		?>

		<div class="container-fluid background2 services_page">
			<div class="div_rgba2"></div>
				<?php
					include('/elements/meniu.php');
				?>
			<div class="div_features">
				<p class="text-center">Customer <strong>Service</strong></p>
			</div>
		</div>

		<div class="container-fluid container_service">
					
			<div class="website_features">
				<span class="span_normal"><strong>FAQ</strong></span>
			</div>

			<div class="clear red_line"></div>

			<div class="div_payment">SimpleSite subscriptions and Payment</div>

			<div class="general_use">General use of SimpleSite</div>

			<div class="clear"></div>

			<div class="div_faq">
				<p>How much does a SimpleSite subscription cost and what features are included?</p>
			</div>

			<div class="div_faq">
				<p>What if I want more pages or photos on my website?</p>
			</div>

			<div class="div_faq">
				<p>How can I get my own domain name?</p>
			</div>

			<div class="div_faq">
				<p>I have received a SimpleSite gift certificate. How do I redeem it?</p>
			</div>

			<div class="div_faq">
				<p>Can I renew a current PRO subsciption with a gift certificate?</p>
			</div>

			<div class="div_faq">
				<p>How can I pay for or renew my PRO subscription?</p>
			</div>

			<div class="div_faq">
				<p>My website is offline. Why?</p>
			</div>

			<div class="div_faq">
				<p>What happens when my PRO subscitpion expires?</p>
			</div>

			<div class="div_faq">
				<p>What happens if I don`t want to renew my PRO subscription?</p>
			</div>

			<div class="div_faq">
				<p>How many days do I have left before my PRO subscitpion expires?</p>
			</div>

			<div class="website_features">
				<span class="span_normal"><strong>CUSTOMER SERVICE</strong></span>
			</div>

			<div class="clear red_line"></div>

			<div class="div_email">
				<p>Email:</p> <span>customerservice@simplesite.com</span>
			</div>

			<div class="div_general1">
				<p>We try to respond to all requests within two business days.</p>
				<p>Please describe your issue and include the name of your website to make it easier for us to help you.</p>
			</div>

			<div class="div_general2">
				<a href="/web/PHP.php" title="Home"><span>SimpleSite.com</span></a>
				<p>Toldbodgade 31, 3.tv</p>
				<p>1253 Conpenhagen K.</p>
				<p>Denmark</p>
				<p>VAT Reg No: DK27 97 24 54</p>
				<p>Owned by SimpleSite ApS VAT Reg No DK10079861</p>
			</div>

		</div>

		<?php
			include('/elements/footer.php');
		?>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script type="text/javascript" src="/web/js/bootstrap.min.js"></script>
	</body>

</html>