<!DOCTYPE html>

<html>

	<?php
		include('/elements/head.php');
	?>

	<body>
		
		<?php
			include('/elements/header_1.php');
		?>

		<div class="news" title="Socialize">
			<i class="fa fa-cogs"></i>

			<div class="FB shou" title="Facebook.com">
				<a target="blank" href="https://www.facebook.com/">
					<i class="fa fa-facebook-square" aria-hidden="true"></i>
				</a>
			</div>
			
			<div class="TW shou" title="Twitter.com">
				<a target="blank" href="https://www.twitter.com/">
					<i class="fa fa-twitter-square" aria-hidden="true"></i>
				</a>
			</div>

			<div class="LI shou" title="LinkedIn.com">
				<a target="blank" href="https://ro.linkedin.com/">
					<i class="fa fa-linkedin-square" aria-hidden="true"></i>
				</a>
			</div>
		</div>

		<div class="container-fluid div_relative padding0">

			
			<?php
				include('/elements/header_2.php');
			?>	

			<div class="header_slider">
				<div class="bckg_img_index1">
					<div class="div_rgba"></div>			

					<div class="container div_info div_1200 padding_left0 padding_right0">
						<h1><b>Free updates</b></h1>
						<h1><b>Top notch Support</b></h1>
						<p><b>AND SO MUCH MORE...</b></p>
						<div class="div_butzon">
							SEE FEATURES
						</div>
					</div>
				</div>

				<div class="bckg_img_index2">
					<div class="div_rgba"></div>

					<div class="container div_info div_1200 padding_left0 padding_right0">
						<h1><b>Free chocholate</b></h1>
						<h1><b>Top notch Fiddling</b></h1>
						<p><b>AND SO MUCH MORE...</b></p>
						<div class="div_butzon">
							SEE FEATURES
						</div>
					</div>
				</div>

				<div class="bckg_img_index3">
					<div class="div_rgba"></div>

					<div class="container div_info div_1200 padding_left0 padding_right0">
						<h1><b>Free lunch</b></h1>
						<h1><b>Top notch Delicious</b></h1>
						<p><b>AND SO MUCH MMMM...</b></p>
						<div class="div_butzon">
							SEE FEATURES
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="container-fluid div_relative padding0">

			<div class="div_purchase"><b>PURCHASE</b></div>

			<div class="container mobile_container padding_top_bottom padding_left0 padding_right0">
				<div class="col-md-4 div_features padding_left0">
					<i class="fa fa-mobile"></i>
					<h4>Responsive Design</h4>
					<div class="red_line"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Ab corrupti, quasi? Beatae cumque maiores.</p>
				</div>

				<div class="col-md-4 div_features">
					<i class="fa fa-gift"></i>
					<h4>Premium plugins</h4>
					<div class="red_line"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Ab corrupti, quasi? Beatae cumque maiores.</p>
				</div>

				<div class="col-md-4 div_features padding_right0">
					<i class="fa fa-code"></i>
					<h4>Free updates</h4>
					<div class="red_line"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Ab corrupti, quasi? Beatae cumque maiores.</p>
				</div>
			</div>
		</div>

		<div class="container-fluid div_height padding0">
			<div class="col-md-6 bckg_img2">
				<div class="div_rgba"></div>
			</div>

			<div class="col-md-6 bckg_color3 padding0">
				<div class="col-md-offset-1 col-md-11 mobile_height">
					<div class="col-md-10 div_us">
						<h3>About us</h3>
						<div class="red_line_us"></div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste sunt cupiditate asperiores atque consectetur reiciendis, ex voluptate! Atque distinctio, illum minus consectetur quae explicabo eveniet dolorum voluptatum excepturi autem illo, laudantium iusto itaque vero.</p>
					</div>

					<div class="col-md-10 div_clients">
						<h4><i class="fa fa-heartbeat"></i> Happy Clients</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis explicabo inventore, nisi sint consectetur distinctio! Asperiores totam dolrum, odio ex?</p>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid padding0">
			<div class="container div_options padding_left0 padding_right0">
				<div class="col-md-6 div_opt_ct padding_left0">
					<div class="div_icon">
						<i class="fa fa-bolt"></i>
					</div>
					
					<div class="div_inline">
						<h4>Top notch Support</h4>
						<a href="">read more</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
					</div>
					<div class="clear"></div>
				</div>

				<div class="col-md-6 div_opt_ct padding_right0">
					<div class="div_icon">
						<i class="fa fa-bomb"></i>
					</div>
					
					<div class="div_inline">
						<h4>SEO Ready</h4>
						<a href="">read more</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
					</div>
					<div class="clear"></div>
				</div>

				<div class="col-md-6 div_opt_ct padding_left0 margin_bottom0">
					<div class="div_icon">
						<i class="fa fa-check-square"></i>
					</div>
					
					<div class="div_inline">
						<h4>Bootstrap Based</h4>
						<a href="">read more</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
					</div>
					<div class="clear"></div>
				</div>

				<div class="col-md-6 div_opt_ct padding_right0 margin_bottom0">
					<div class="div_icon">
						<i class="fa fa-rocket"></i>
					</div>
					
					<div class="div_inline">
						<h4>Working widgets</h4>
						<a href="">read more</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<div class="container-fluid div_flag_container padding0">
			<div class="col-md-4 div_flag1 flag_mobile">
				<h3>Customizable</h3>
				<a href="">read more</a>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum recusandae sequi atque voluptatum saepe suscipit veritatis voluptas!</p>
				<span>1</span>
			</div>

			<div class="col-md-4 div_flag2 flag_mobile">
				<h3>Easy to use</h3>
				<a href="">read more</a>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum recusandae sequi atque voluptatum saepe suscipit veritatis voluptas!</p>
				<span>2</span>
			</div>

			<div class="col-md-4 div_flag3 flag_mobile">
				<h3>Clean code</h3>
				<a href="">read more</a>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum recusandae sequi atque voluptatum saepe suscipit veritatis voluptas!</p>
				<span>3</span>
			</div>
		</div>

		<div class="container-fluid padding0">
			<div class="container div_projects padding_left0 padding_right0">
				<div class="col-md-12 padding0">
					<div class="div_our_projects">
						<h3>Our Projects</h3>
						<div class="red_line_pro"></div>
					</div>

					<div class="div_list">
						<ul>
							<li><i class="fa fa-bars"></i></li>
							<li><a href="">Above</a></li>
							<li>/</li>
							<li><a href="">Beyond</a></li>
							<li>/</li>
							<li><a href="" style="padding-right: 0">Nowhere</a></li>
						</ul>
					</div>
					<div class="clear"></div>
				</div>

				<div class="col-md-12 div_img_container padding0 margin_top0">
					<div class="div_img">
						<div class="rgba_hover">
							<h3>Above and beyond</h3>
							<div class="red_line_rgba_hover"></div>
							<p class="skill">design & photography</p>
							<br>
							<p class="date">15 JUNE</p>
						</div>

						<img src="/web/project/img/p01.jpg">
					</div>
					<div class="div_img">
						<div class="rgba_hover">
							<h3>Up and down</h3>
							<div class="red_line_rgba_hover"></div>
							<p class="skill">design & photography</p>
							<br>
							<p class="date">01 SEPTEMBER</p>
						</div>

						<img src="/web/project/img/p11.jpg">
					</div>
					<div class="div_img">
						<div class="rgba_hover">
							<h3>Climbing and descending</h3>
							<div class="red_line_rgba_hover"></div>
							<p class="skill">design & photography</p>
							<br>
							<p class="date">05 MAY</p>
						</div>

						<img src="/web/project/img/p03.jpg">
					</div>
				</div>

				<div class="clear"></div>

				<div class="col-md-12 div_img_container padding0">
					<div class="div_img">
						<div class="rgba_hover">
							<h3>Working and enjoying</h3>
							<div class="red_line_rgba_hover"></div>
							<p class="skill">design & photography</p>
							<br>
							<p class="date">26 APRIL</p>
						</div>

						<img src="/web/project/img/p04.jpg">
					</div>
					<div class="div_img">
						<div class="rgba_hover">
							<h3>Everywhere and nowhere</h3>
							<div class="red_line_rgba_hover"></div>
							<p class="skill">design & photography</p>
							<br>
							<p class="date">30 JULY</p>
						</div>

						<img src="/web/project/img/p05.jpg">
					</div>
					<div class="div_img">
						<div class="rgba_hover">
							<h3>Fast and furious</h3>
							<div class="red_line_rgba_hover"></div>
							<p class="skill">design & photography</p>
							<br>
							<p class="date">25 MARCH</p>
						</div>

						<img src="/web/project/img/p06.jpg">
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="container-fluid div_team_container padding0">
			<h3>The team behind all this</h3>
			<div class="red_line_team"></div>

			<div class="div_slider padding0">
				<div class="box_img">
					<i class="fa fa-arrows-h"></i>
					<img src="/web/project/img/team1.jpg">
				</div>

				<div class="box_img box_mobile">
					<i class="fa fa-arrows-h"></i>
					<img src="/web/project/img/team2.jpg">
				</div>

				<div class="box_img box_mobile">
					<i class="fa fa-arrows-h"></i>
					<img src="/web/project/img/team3.jpg">
				</div>

				<div class="box_img box_mobile">
					<i class="fa fa-arrows-h"></i>
					<img src="/web/project/img/team4.jpg">
				</div>

				<div class="box_img box_mobile">
					<i class="fa fa-arrows-h"></i>
					<img src="/web/project/img/team5.jpg">
				</div>

				<div class="box_img box_mobile">
					<i class="fa fa-arrows-h"></i>
					<img src="/web/project/img/team1.jpg">
				</div>

				<div class="box_img box_mobile">
					<i class="fa fa-arrows-h"></i>
					<img src="/web/project/img/team2.jpg">
				</div>

				<div class="box_img box_mobile">
					<i class="fa fa-arrows-h"></i>
					<img src="/web/project/img/team3.jpg">
				</div>


				<div class="box_img box_mobile">
					<i class="fa fa-arrows-h"></i>
					<img src="/web/project/img/team4.jpg">
				</div>

				<div class="box_img box_mobile">
					<i class="fa fa-arrows-h"></i>
					<img src="/web/project/img/team5.jpg">
				</div>
			</div>

			<div class="clear"></div>

			<div class="container padding0">
				<div class="col-md-3 div_stamp padding0">
					<img src="/web/project/img/client01a">
				</div>

				<div class="col-md-3 div_stamp padding0">
					<img src="/web/project/img/client02a">
				</div>

				<div class="col-md-3 div_stamp padding0">
					<img src="/web/project/img/client03a">
				</div>

				<div class="col-md-3 div_stamp padding0">
					<img src="/web/project/img/client04a">
				</div>

				<div class="col-md-3 div_stamp padding0">
					<img src="/web/project/img/client05a">
				</div>

				<div class="col-md-3 div_stamp padding0">
					<img src="/web/project/img/client06a">
				</div>

				<div class="col-md-3 div_stamp padding0">
					<img src="/web/project/img/client07a">
				</div>

				<div class="col-md-3 div_stamp padding0">
					<img src="/web/project/img/client08a">
				</div>

			</div>
		</div>

		<div class="container-fluid padding0">
			<div class="col-md-12 mobile_order div_cite_test padding0">
				<div class="col-md-8 div_cite">
					<p>
						<font color="#667">
							<cite>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, quidem nesciunt totam? Quia, facere nobis fuga. Voluptas, sit maxime accusantium ipsum dolor.</cite>
						</font>
					</p>
					<p><font color="#667"><b>Mark Doe</b></font> &nbsp; &nbsp; <font color="darkred" size="-1"><b>AudioJungle</b></font></p>
				</div>

				<div class="col-md-4 div_test">
					<div class="red_line_test"></div>
					<h3>Testimonials</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum recusandae sequi atque voluptatum saepe!</p>
					<i class="fa fa-quote-right"></i>
				</div>
			</div>

			<div class="clear"></div>

			<div class="container mobile_hover padding_top_bottom padding_right0 padding_left0">
				<div class="col-md-4 div_hover">
					<h4><i class="fa fa-bolt"></i> <b>TOP NOTCH SUPPORT</b></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis maiores repudiandae, accusantium reiciendis!</p>
					<div class="red_line_hover"></div>
				</div>

				<div class="col-md-4 div_hover">
					<h4><i class="fa fa-cart-plus"></i> <b>ECOMMERCE</b></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis maiores repudiandae, accusantium reiciendis!</p>
					<div class="red_line_hover"></div>
				</div>

				<div class="col-md-4 div_hover">
					<h4><i class="fa fa-shield"></i> <b>RELIABLE</b></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis maiores repudiandae, accusantium reiciendis!</p>
					<div class="red_line_hover"></div>
				</div>
			</div>
		</div>

		<div class="to_top">
			<i class="fa fa-thumbs-up" aria-hidden="true"></i>
			<br>
			<p>to Top?</p>
		</div>

		<?php 
			include('/elements/footer.php');
		?>
	
		<?php
			include('/elements/scripts.php');
		?>
	</body>

</html>