<!DOCTYPE html>

<html>

	<?php
		include('/elements/head.php');
	?>

	<body>

		<?php
			include('/elements/header_1.php');
		?>

		<div class="news" title="Socialize">
			<i class="fa fa-cogs"></i>

			<div class="FB shou" title="Facebook.com">
				<a target="blank" href="https://www.facebook.com/">
					<i class="fa fa-facebook-square" aria-hidden="true"></i>
				</a>
			</div>
			
			<div class="TW shou" title="Twitter.com">
				<a target="blank" href="https://www.twitter.com/">
					<i class="fa fa-twitter-square" aria-hidden="true"></i>
				</a>
			</div>

			<div class="LI shou" title="LinkedIn.com">
				<a target="blank" href="https://ro.linkedin.com/">
					<i class="fa fa-linkedin-square" aria-hidden="true"></i>
				</a>
			</div>
		</div>

		<div class="container-fluid bckg_img_services1">
			<div class="div_rgba"></div>

			<?php
				include('/elements/header_2.php');
			?>

			<div class="container div_1200 padding_left0 padding_right0">
				<div class="col-md-5 div_serve padding0">
					<h1>Services</h1>
					<div class="red_line_services"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum aset!</p>
				</div>
			</div>
		</div>

		<div class="container-fluid div_relative padding0">

			<div class="div_purchase"><b>PURCHASE</b></div>

			<div class="container mobile_services padding_top_bottom padding_left0 padding_right0">
				<div class="col-md-4 div_features padding_left0" style="border-right: 0.5px solid silver;">
					<i class="fa fa-gift"></i>
					<h4>Free updates</h4>
					<div class="red_line"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Ab corrupti, quasi? Beatae cumque maiores.</p>
				</div>

				<div class="col-md-4 div_features" style="border-left: 0.5px solid silver; border-right: 0.5px solid silver;">
					<i class="fa fa-bolt"></i>
					<h4>Premium Support</h4>
					<div class="red_line"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Ab corrupti, quasi? Beatae cumque maiores.</p>
				</div>

				<div class="col-md-4 div_features padding_right0" style="border-left: 0.5px solid silver;">
					<i class="fa fa-code"></i>
					<h4>Documentation</h4>
					<div class="red_line"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Ab corrupti, quasi? Beatae cumque maiores.</p>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="container-fluid div_services_pic padding0">
			<div class="col-md-4 div_services_ct1 services_mobile">
				<div class="div_rgba"></div>
				<div class="div_services_relative">
					<h3>Work Hard</h3>
					<div class="red_line_pic"></div>
					<a href="">read more</a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum recusandae sequi atque voluptatum saepe suscipit veritatis voluptas! Odit, eos, consectetur.</p>
				</div>
			</div>

			<div class="col-md-4 div_services_ct2 services_mobile">
				<div class="div_rgba"></div>
				<div class="div_services_relative">
					<h3>Play Hard</h3>
					<div class="red_line_pic"></div>
					<a href="">read more</a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum recusandae sequi atque voluptatum saepe suscipit veritatis voluptas! Odit, eos, consectetur.</p>
				</div>
			</div>

			<div class="col-md-4 div_services_ct3 services_mobile">
				<div class="div_rgba"></div>
				<div class="div_services_relative">
					<h3>and Have Fun</h3>
					<div class="red_line_pic"></div>
					<a href="">read more</a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum recusandae sequi atque voluptatum saepe suscipit veritatis voluptas! Odit, eos, consectetur.</p>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="container-fluid padding0">
			<div class="container mobile_features padding_top_bottom padding_left0 padding_right0">
				<div class="col-md-6 div_services padding_left0">
					<div class="col-md-1 div_services_icon padding0">
						<i class="fa fa-mobile"></i>
					</div>

					<div class="col-md-10">
						<h4>Responsive Design</h4>
						<a href="">read more</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
					</div>
				</div>

				<div class="col-md-6 div_services padding_right0">
					<div class="col-md-1 div_services_icon padding0">
						<i class="fa fa-cog"></i>
					</div>

					<div class="col-md-10">
						<h4>Bootstrap based</h4>
						<a href="">read more</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
					</div>
				</div>

				<div class="col-md-6 div_services padding_left0">
					<div class="col-md-1 div_services_icon padding0">
						<i class="fa fa-bomb"></i>
					</div>

					<div class="col-md-10">
						<h4>Working widgets</h4>
						<a href="">read more</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
					</div>
				</div>

				<div class="col-md-6 div_services padding_right0">
					<div class="col-md-1 div_services_icon padding0">
						<i class="fa fa-rocket"></i>
					</div>

					<div class="col-md-10">
						<h4>Customizable</h4>
						<a href="">read more</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
					</div>
				</div>

				<div class="col-md-6 div_services padding_left0">
					<div class="col-md-1 div_services_icon padding0">
						<i class="fa fa-cart-plus"></i>
					</div>

					<div class="col-md-10">
						<h4>Ecommerce</h4>
						<a href="">read more</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
					</div>
				</div>

				
				<div class="col-md-6 div_services padding_right0">
					<div class="col-md-1 div_services_icon padding0">
						<i class="fa fa-shield"></i>
					</div>

					<div class="col-md-10">
						<h4>Reliable</h4>
						<a href="">read more</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="to_top">
			<i class="fa fa-thumbs-up" aria-hidden="true"></i>
			<br>
			<p>to Top?</p>
		</div>

		<?php 
			include('/elements/footer.php');
		?>

		<?php
			include('/elements/scripts.php');
		?>
	</body>

</html>