<!DOCTYPE html>

<html>

	<?php
		include('/elements/head.php');
	?>

	<body>

		<?php
			include('/elements/header_1.php');
		?>

		<div class="news" title="Socialize">
			<i class="fa fa-cogs"></i>

			<div class="FB shou" title="Facebook.com">
				<a target="blank" href="https://www.facebook.com/">
					<i class="fa fa-facebook-square" aria-hidden="true"></i>
				</a>
			</div>
			
			<div class="TW shou" title="Twitter.com">
				<a target="blank" href="https://www.twitter.com/">
					<i class="fa fa-twitter-square" aria-hidden="true"></i>
				</a>
			</div>

			<div class="LI shou" title="LinkedIn.com">
				<a target="blank" href="https://ro.linkedin.com/">
					<i class="fa fa-linkedin-square" aria-hidden="true"></i>
				</a>
			</div>
		</div>

		<div class="container-fluid bckg_img_about">
			<div class="div_rgba"></div>

			<?php
				include('/elements/header_2.php');
			?>

			<div class="container div_1200 padding_left0 padding_right0">
				<div class="col-md-5 div_about_us padding0">
					<h1>About us</h1>
					<div class="red_line_services"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum amet!</p>
				</div>

				<div class="clear"></div>

				<div class="div_about_links">
					<ul>
						<li><a href="">Home</a></li>
						<li>/</li>
						<li><a href="">Pages</a></li>
						<li>/</li>
						<li><a href="">About</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="container-fluid div_relative padding0">

			<div class="div_purchase"><b>PURCHASE</b></div>

			<div class="container mobile_container padding_top_bottom padding_left0 padding_right0">
				<div class="col-md-4 div_features padding_left0">
					<i class="fa fa-globe"></i>
					<h4>Everywhere</h4>
					<div class="red_line"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Ab corrupti, quasi? Beatae cumque maiores.</p>
				</div>

				<div class="col-md-4 div_features">
					<i class="fa fa-shield"></i>
					<h4>Reliable</h4>
					<div class="red_line"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Ab corrupti, quasi? Beatae cumque maiores.</p>
				</div>

				<div class="col-md-4 div_features padding_right0">
					<i class="fa fa-comments"></i>
					<h4>We help you</h4>
					<div class="red_line"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Ab corrupti, quasi? Beatae cumque maiores.</p>
				</div>
			</div>
		</div>

		<div class="container-fluid div_services_pic padding0">
			<div class="col-md-4 div_about_ct1 div_about_mobile">
				<div class="div_rgba"></div>
				<div class="div_services_relative">
					<h3>EMPOWERING</h3>
					<div class="red_line_about"></div>
					<a href="">read more</a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum recusandae sequi atque voluptatum saepe suscipit veritatis voluptas! Odit, eos, consectetur.</p>
				</div>
			</div>

			<div class="col-md-4 div_about_ct2 div_about_mobile">
				<div class="div_rgba"></div>
				<div class="div_services_relative">
					<h3>INSPIRING</h3>
					<div class="red_line_about"></div>
					<a href="">read more</a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum recusandae sequi atque voluptatum saepe suscipit veritatis voluptas! Odit, eos, consectetur.</p>
				</div>
			</div>

			<div class="col-md-4 div_about_ct3 div_about_mobile">
				<div class="div_rgba"></div>
				<div class="div_services_relative">
					<h3>REWARDING</h3>
					<div class="red_line_about"></div>
					<a href="">read more</a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum recusandae sequi atque voluptatum saepe suscipit veritatis voluptas! Odit, eos, consectetur.</p>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="container-fluid padding0">
			<div class="container mobile_motto padding_left0 padding_right0" style="margin-top: 125px; margin-bottom: 125px;">
				<div class="col-md-6 div_laptop padding_left0">
					<img src="/web/project/img/img01.png">
				</div>

				<div class="col-md-6 div_motto padding_right0">
					<div class="col-md-12 div_motto_ct padding0">
						<div class="div_icon_about">
							<i class="fa fa-wrench"></i>
						</div>
						
						<div class="div_inline_about">
							<h4>Work Hard</h4>
							<a href="">read more</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
						</div>
						<div class="clear"></div>
					</div>

					<div class="col-md-12 div_motto_ct padding0">
						<div class="div_icon_about">
							<i class="fa fa-heart"></i>
						</div>
						
						<div class="div_inline_about">
							<h4>Play Hard</h4>
							<a href="">read more</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid div_bckg_stat padding0">
			<div class="container padding_left0 padding_right0">
				<div class="div_statistics">
					<i class="fa fa-files-o" aria-hidden="true"></i>
					<br>
					<span>123</span>
					<div class="red_line_stat"></div>
					<p>Web Projects</p>
					
				</div>

				<div class="div_statistics">
					<i class="fa fa-thumbs-up" aria-hidden="true"></i>
					<br>
					<span>1850</span>
					<div class="red_line_stat"></div>
					<p>Happy Customers</p>
				</div>

				<div class="div_statistics">
					<i class="fa fa-universal-access" aria-hidden="true"></i>
					<br>
					<span>1768</span>
					<div class="red_line_stat"></div>
					<p>Support Tickets</p>
				</div>

				<div class="div_statistics">
					<i class="fa fa-users" aria-hidden="true"></i>
					<br>
					<span>58</span>
					<div class="red_line_stat"></div>
					<p>Employees</p>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="container-fluid div_bckg_logos padding0">
			<div class="div_rgba"></div>

			<div class="container mobile_logo padding_left0 padding_right0" style="padding-top: 125px;">
				<div class="col-md-12 div_logos_ct padding0">
					<div class="col-md-3 div_logo padding0">
						<img src="/web/project/img/client01a.png">
					</div>

					<div class="col-md-3 div_logo padding0">
						<img src="/web/project/img/client02a.png">
					</div>

					<div class="col-md-3 div_logo padding0">
						<img src="/web/project/img/client03a.png">
					</div>

					<div class="col-md-3 div_logo padding0">
						<img src="/web/project/img/client04a.png">
					</div>
					<div class="clear"></div>
				</div>

				<div class="col-md-12 div_logos_ct padding0">
					<div class="col-md-3 div_logo padding0">
						<img src="/web/project/img/client05a.png">
					</div>

					<div class="col-md-3 div_logo padding0">
						<img src="/web/project/img/client06a.png">
					</div>

					<div class="col-md-3 div_logo padding0">
						<img src="/web/project/img/client07a.png">
					</div>

					<div class="col-md-3 div_logo padding0">
						<img src="/web/project/img/client08a.png">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<div class="container-fluid padding0">
			<div class="container mobile_about_support padding_top_bottom padding_left0 padding_right0">
				<div class="col-md-6 div_about_list padding_left0">
					<ul>
						<li>
							<h4 class="about"><i class="fa fa-angle-down"></i> How to create an account</h4>
							<p class="text_about">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga!</p>
						</li>
						<li>
							<h4 class="about"><i class="fa fa-angle-down"></i> How to change username</h4>
							<p class="text_about">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga!</p>
						</li>
						<li>
							<h4 class="about"><i class="fa fa-angle-down"></i> Account removal</h4>
							<p class="text_about">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga!</p>
						</li>
						<li>
							<h4 class="about"><i class="fa fa-angle-down"></i> Forgot password</h4>
							<p class="text_about">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga!</p>
						</li>
					</ul>
				</div>

				<div class="col-md-6 mobile_choose padding_right0">
					<div class="col-md-12 div_choose padding0">
						<h3>Why choose us</h3>
						<div class="red_line_choose"></div>
						<p>Lorem ipsum dolor sit amet, consect adipisicing elit. Dolores facilis nostrum facere illo repudiandae dicta fuga quasi provident adipisci fugiat repellat atque sunt nulla.</p>
					</div>

					<div class="col-md-12 div_support padding0">
						<h3><i class="fa fa-bolt"></i> PREMIUM SUPPORT</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis maiores repudiandae, accusantium reiciendis provident adipisci fugiat repellat atque sunt nulla!</p>
						<div class="red_line_choose"></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="to_top">
			<i class="fa fa-thumbs-up" aria-hidden="true"></i>
			<br>
			<p>to Top?</p>
		</div>

		<?php 
			include('/elements/footer.php');
		?>
	
		<?php
			include('/elements/scripts.php');
		?>
	</body>

</html>