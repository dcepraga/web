<!DOCTYPE html>

<html>

	<?php
		include('/elements/head.php');
	?>

	<body>

		<?php
			include('/elements/header_1.php');
		?>

		<div class="news" title="Socialize">
			<i class="fa fa-cogs"></i>

			<div class="FB shou" title="Facebook.com">
				<a target="blank" href="https://www.facebook.com/">
					<i class="fa fa-facebook-square" aria-hidden="true"></i>
				</a>
			</div>
			
			<div class="TW shou" title="Twitter.com">
				<a target="blank" href="https://www.twitter.com/">
					<i class="fa fa-twitter-square" aria-hidden="true"></i>
				</a>
			</div>

			<div class="LI shou" title="LinkedIn.com">
				<a target="blank" href="https://ro.linkedin.com/">
					<i class="fa fa-linkedin-square" aria-hidden="true"></i>
				</a>
			</div>
		</div>

		<div class="container-fluid bckg_img_contact">
			<div class="div_rgba"></div>

			<?php
				include('/elements/header_2.php');
			?>

			<div class="container div_1200 padding_left0 padding_right0">
				<div class="col-md-5 div_contact_header padding0">
					<h1>Get in Touch</h1>
					<div class="red_line_services"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum aset!</p>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="container-fluid padding0">
			<div class="col-md-12 div_contact_flag_container padding0">
				<div class="col-md-3 div_contact_flag1">
					<p>55 Cyan Avenue, Suite 65</p>
					<br>
					<p>Los Angeles, CA 8008</p>
					<i class="fa fa-compass"></i>
				</div>

				<div class="col-md-3 div_contact_flag2">
					<p>support@palas.com</p>
					<br>
					<p>billing@pala.com</p>
					<i class="fa fa-paper-plane"></i>
				</div>

				<div class="col-md-3 div_contact_flag3">
					<p>0 800-55-22-55</p>
					<br>
					<p>0 500-22-44-55</p>
					<i class="fa fa-phone"></i>
				</div>

				<div class="col-md-3 div_contact_flag4">
					<p>Mon - Fri:&nbsp; 9:00 - 18:00</p>
					<br>
					<p>Sat - Sun:&nbsp; Closed</p>
					<i class="fa fa-bell"></i>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="clear"></div>

		<div class="container-fluid div_relative padding0">
			<div class="div_purchase_contact"><b>PURCHASE</b></div>

			<div class="container div_mobile_contact padding_top_bottom padding_left0 padding_right0">
				<div class="col-md-7 div_form padding_left0">
					<form action="" method="get" enctype="" name="contact_form">
						<input type="text" name="first_name" placeholder="First Name">
						<input type="text" name="last_name" placeholder="Last Name">
						<input type="text" name="subject" placeholder="Subject *">
						<input type="text" name="e_mail" placeholder="Email Address *">
						<textarea name="message_contact" placeholder="Message *"></textarea>
						<div class="g-recaptcha" data-sitekey="6LdyvFEUAAAAAKGyiLSvjcTLLtbh_XnKOlMGYKzt"></div>
						<button type="submit" class="button_submit" name="Submit" value="submit">SUBMIT</button>
					</form>
				</div>

				<div class="col-md-5 div_contact padding_right0">
					<div class="col-md-12 padding0" style="margin-bottom: 50px;">
						<h3>Contact us</h3>
						<div class="red_line_contact"></div>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam quasi modi delectus aliquid doloribus. 
							Accusantium iste earum saepe provident sapiente fugit, vel perspiciatis harum, tempore id, porro sequi aliquid.
						</p>
					</div>

					<div class="col-md-12 padding0">
						<h3>Get social</h3>
						<div class="red_line_contact"></div>
						<div class="col-md-8 div_social padding0" style="margin-top: 25px;">
							<div class="col-md-1 div_social_icon padding0">
								<a target="blank" href="https://www.twitter.com/" title="Twitter.com">
									<i class="fa fa-twitter"></i>
								</a>
							</div>
							<div class="col-md-4 padding0">
								<a target="blank" href="https://www.twitter.com/" title="Twitter.com">
									<span class="span_hover">Follow us</span>
								</a>
							</div>
							<span class="span_right">455 followers</span>
						</div>

						<div class="col-md-8 div_social padding0">
							<div class="col-md-1 div_social_icon padding0">
								<a target="blank" href="https://www.facebook.com/" title="Facebook.com">
									<i class="fa fa-facebook-f"></i>
								</a>
							</div>
							<div class="col-md-4 padding0">
								<a target="blank" href="https://www.facebook.com/" title="Facebook.com">
									<span class="span_hover">Like us</span>
								</a>
							</div>
							<span class="span_right">505 likes</span>
						</div>

						<div class="col-md-8 div_social padding0">
							<div class="col-md-1 div_social_icon padding0">
								<a href="" title="Enter ur email">
									<i class="fa fa-envelope"></i>
								</a>
							</div>
							<div class="col-md-4 padding0">
								<a href="" title="Enter ur email">
									<span class="span_hover">Subscribe</span>
								</a>
							</div>
							<span class="span_right">250 subscribers</span>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="clear"></div>

		<div class="container-fluid mobile_iframe padding0" style="height: 550px;">
			<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d9350.300467844947!2d-118.24129357117488!3d34.04821701611357!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sro!2sro!4v1523118640599" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>

		<div class="clear"></div>

		<div class="to_top">
			<i class="fa fa-thumbs-up" aria-hidden="true"></i>
			<br>
			<p>to Top?</p>
		</div>

		<?php 
			include('/elements/footer.php');
		?>

		<div class="clear"></div>

		<?php
			include('/elements/scripts.php');
		?>
	</body>

</html>