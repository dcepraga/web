<!DOCTYPE html>

<html>

	<?php
		include('/elements/head.php');
	?>

	<body>

		<?php
			include('/elements/header_1.php');
		?>

		<div class="div_john div_pop_up">
			<i class="fa fa-times" title="Close"></i>
			<div class="div_inner">
				<img src="/web/project/img/team1.jpg">
			</div>
		</div>

		<div class="div_ines div_pop_up">
			<i class="fa fa-times" title="Close"></i>
			<div class="div_inner">
				<img src="/web/project/img/team2.jpg">
			</div>
		</div>

		<div class="div_alex div_pop_up">
			<i class="fa fa-times" title="Close"></i>
			<div class="div_inner">
				<img src="/web/project/img/team6.jpg">
			</div>
		</div>

		<div class="div_chris div_pop_up">
			<i class="fa fa-times" title="Close"></i>
			<div class="div_inner">
				<img src="/web/project/img/team4.jpg">
			</div>
		</div>

		<div class="div_jane div_pop_up">
			<i class="fa fa-times"></i>
			<div class="div_inner" title="Close">
				<img src="/web/project/img/team5.jpg">
			</div>
		</div>

		<div class="div_joe div_pop_up">
			<i class="fa fa-times" title="Close"></i>
			<div class="div_inner">
				<img src="/web/project/img/team3.jpg">
			</div>
		</div>

		<div class="news" title="Socialize">
			<i class="fa fa-cogs"></i>

			<div class="FB shou" title="Facebook.com">
				<a target="blank" href="https://www.facebook.com/">
					<i class="fa fa-facebook-square" aria-hidden="true"></i>
				</a>
			</div>
			
			<div class="TW shou" title="Twitter.com">
				<a target="blank" href="https://www.twitter.com/">
					<i class="fa fa-twitter-square" aria-hidden="true"></i>
				</a>
			</div>

			<div class="LI shou" title="LinkedIn.com">
				<a target="blank" href="https://ro.linkedin.com/">
					<i class="fa fa-linkedin-square" aria-hidden="true"></i>
				</a>
			</div>
		</div>

		<div class="container-fluid bckg_img_team">
			<div class="div_rgba"></div>

			<?php
				include('/elements/header_2.php');
			?>

			<div class="container div_1200 padding_left0 padding_right0">
				<div class="col-md-5 div_our_team padding0">
					<h1>Our Team</h1>
					<div class="red_line_services"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum amet!</p>
				</div>

				<div class="clear"></div>

				<div class="div_team_links">
					<ul>
						<li><a href="">Home</a></li>
						<li>/</li>
						<li><a href="">Pages</a></li>
						<li>/</li>
						<li><a href="">Team</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="container-fluid div_relative padding0">

			<div class="div_purchase_faq"><b>PURCHASE</b></div>	

			<div class="container mobile_team padding_top_bottom padding_left0 padding_right0">
			
				<div class="div_team">
					<div class="col-md-12 div_team_pic padding0" data-value="div_john" title="Click to enlarge">
						<img src="/web/project/img/team1.jpg">
					</div>
			
					<div class="col-md-12 div_undertitle">
						<span class="span_name">John Doe</span>
			
						<div class="div_media padding0">
							<a target="blank" href="https://www.twitter.com/" title="Twitter.com"><i class="fa fa-twitter"></i></a>
							<a target="blank" href="https://dribbble.com/" title="Dribbble.com"><i class="fa fa-dribbble"></i></a>
							<a target="blank" href="https://www.behance.net/" title="Behance.net"><i class="fa fa-behance"></i></a>
						</div>
					</div>

					<div class="clear"></div>
			
					<div class="col-md-12">
						<span class="span_occupation">web designer</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						<p style="margin-bottom: 0;">Repudiandae quae aut fugit neque ducimus labore!</p>
					</div>
				</div>
				
				<div class="div_team">
					<div class="col-md-12 div_team_pic padding0" data-value="div_ines" title="Click to enlarge">
						<img src="/web/project/img/team2.jpg">
					</div>
			
					<div class="col-md-12 div_undertitle">
						<span class="span_name">Ines Doe</span>
			
						<div class="div_media padding0">
							<a target="blank" href="https://www.twitter.com/" title="Twitter.com"><i class="fa fa-twitter"></i></a>
							<a target="blank" href="https://dribbble.com/" title="Dribbble.com"><i class="fa fa-dribbble"></i></a>
							<a target="blank" href="https://www.behance.net/" title="Behance.net"><i class="fa fa-behance"></i></a>
						</div>
					</div>

					<div class="clear"></div>
			
					<div class="col-md-12">
						<span class="span_occupation">web developer</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						<p style="margin-bottom: 0;">Repudiandae quae aut fugit neque ducimus labore!</p>
					</div>
				</div>
			
				<div class="div_team">
					<div class="col-md-12 div_team_pic padding0" data-value="div_alex" title="Click to enlarge">
						<img src="/web/project/img/team6.jpg">
					</div>
			
					<div class="col-md-12 div_undertitle">
						<span class="span_name">Alex Doe</span>
			
						<div class="div_media padding0">
							<a target="blank" href="https://www.twitter.com/" title="Twitter.com"><i class="fa fa-twitter"></i></a>
							<a target="blank" href="https://dribbble.com/" title="Dribbble.com"><i class="fa fa-dribbble"></i></a>
							<a target="blank" href="https://www.behance.net/" title="Behance.net"><i class="fa fa-behance"></i></a>
						</div>
					</div>

					<div class="clear"></div>
			
					<div class="col-md-12">
						<span class="span_occupation">Manager</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						<p style="margin-bottom: 0;">Repudiandae quae aut fugit neque ducimus labore!</p>
					</div>
				</div>
			
				<div class="div_team" style="margin-top: 75px;">
					<div class="col-md-12 div_team_pic padding0" data-value="div_chris" title="Click to enlarge">
						<img src="/web/project/img/team4.jpg">
					</div>
			
					<div class="col-md-12 div_undertitle">
						<span class="span_name">Chris Doe</span>
			
						<div class="div_media padding0">
							<a target="blank" href="https://www.twitter.com/" title="Twitter.com"><i class="fa fa-twitter"></i></a>
							<a target="blank" href="https://dribbble.com/" title="Dribbble.com"><i class="fa fa-dribbble"></i></a>
							<a target="blank" href="https://www.behance.net/" title="Behance.net"><i class="fa fa-behance"></i></a>
						</div>
					</div>

					<div class="clear"></div>
			
					<div class="col-md-12">
						<span class="span_occupation">graphic designer</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						<p style="margin-bottom: 0;">Repudiandae quae aut fugit neque ducimus labore!</p>
					</div>
				</div>
			
				<div class="div_team" style="margin-top: 75px;">
					<div class="col-md-12 div_team_pic padding0" data-value="div_jane" title="Click to enlarge">
						<img src="/web/project/img/team5.jpg">
					</div>
			
					<div class="col-md-12 div_undertitle">
						<span class="span_name">Jane Doe</span>
			
						<div class="div_media padding0">
							<a target="blank" href="https://www.twitter.com/" title="Twitter.com"><i class="fa fa-twitter"></i></a>
							<a target="blank" href="https://dribbble.com/" title="Dribbble.com"><i class="fa fa-dribbble"></i></a>
							<a target="blank" href="https://www.behance.net/" title="Behance.net"><i class="fa fa-behance"></i></a>
						</div>
					</div>

					<div class="clear"></div>
			
					<div class="col-md-12">
						<span class="span_occupation">web developer</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						<p style="margin-bottom: 0;">Repudiandae quae aut fugit neque ducimus labore!</p>
					</div>
				</div>
			
				<div class="div_team" style="margin-top: 75px;">
					<div class="col-md-12 div_team_pic padding0" data-value="div_joe" title="Click to enlarge">
						<img src="/web/project/img/team3.jpg">
					</div>
			
					<div class="col-md-12 div_undertitle">
						<span class="span_name">Joe Doe</span>
			
						<div class="div_media padding0">
							<a target="blank" href="https://www.twitter.com/" title="Twitter.com"><i class="fa fa-twitter"></i></a>
							<a target="blank" href="https://dribbble.com/" title="Dribbble.com"><i class="fa fa-dribbble"></i></a>
							<a target="blank" href="https://www.behance.net/" title="Behance.net"><i class="fa fa-behance"></i></a>
						</div>
					</div>

					<div class="clear"></div>
			
					<div class="col-md-12">
						<span class="span_occupation">SEO Analytic</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						<p style="margin-bottom: 0;">Repudiandae quae aut fugit neque ducimus labore!</p>
					</div>
					<div class="clear"></div>
				</div>	
			</div>

			<div class="col-md-12 div_team_flag_container padding0">
				<div class="col-md-3 div_team_flag1">
					<h3>Inspiring</h3>
					<a href="">read more</a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Cum recusandae sequi dolorum ipsum asodea!</p>
					<i class="fa fa-compass"></i>
				</div>

				<div class="col-md-3 div_team_flag2">
					<h3>Empowering</h3>
					<a href="">read more</a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Cum recusandae sequi dolorum ipsum asodea!</p>
					<i class="fa fa-heart"></i>
				</div>

				<div class="col-md-3 div_team_flag3">
					<h3>Rewarding</h3>
					<a href="">read more</a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Cum recusandae sequi dolorum ipsum asodea!</p>
					<i class="fa fa-flag-checkered"></i>
				</div>

				<div class="col-md-3 div_team_flag4">
					<h3>and Fun</h3>
					<a href="">read more</a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Cum recusandae sequi dolorum ipsum asodea!</p>
					<i class="fa fa-bicycle"></i>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="to_top">
			<i class="fa fa-thumbs-up" aria-hidden="true"></i>
			<br>
			<p>to Top?</p>
		</div>

		<?php 
			include('/elements/footer.php');
		?>
	
		<?php
			include('/elements/scripts.php');
		?>
	</body>

</html>