<!DOCTYPE html>

<html>

	<?php
		include('/elements/head.php');
	?>

	<body>
		
		<?php
			include('/elements/header_1.php');
		?>

		<div class="news" title="Socialize">
			<i class="fa fa-cogs"></i>

			<div class="FB shou" title="Facebook.com">
				<a target="blank" href="https://www.facebook.com/">
					<i class="fa fa-facebook-square" aria-hidden="true"></i>
				</a>
			</div>
			
			<div class="TW shou" title="Twitter.com">
				<a target="blank" href="https://www.twitter.com/">
					<i class="fa fa-twitter-square" aria-hidden="true"></i>
				</a>
			</div>

			<div class="LI shou" title="LinkedIn.com">
				<a target="blank" href="https://ro.linkedin.com/">
					<i class="fa fa-linkedin-square" aria-hidden="true"></i>
				</a>
			</div>
		</div>

		<div class="container-fluid bckg_img_faq">
			<div class="div_rgba"></div>

			<?php
				include('/elements/header_2.php');
			?>

			<div class="container div_1200 padding_left0 padding_right0">
				<div class="col-md-5 div_faq padding0">
					<h1>FAQs</h1>
					<div class="red_line_services"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum amet!</p>
				</div>

				<div class="clear"></div>

				<div class="div_accounts">
					<ul>
						<li class="tab_click" data-value="set_up">Account Set Up</li>
						<li class="tab_click" data-value="buy">Buying</li>
						<li class="tab_click" data-value="pay">Receive Payments</li>
						<li class="tab_click" data-value="card">Using the card</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="container-fluid div_relative padding0">

			<div class="div_purchase_faq"><b>PURCHASE</b></div>

			<div class="container div_faq_container padding_left0 padding_right0">
				<div class="col-md-9 div_faq_options padding_left0">
					<ul class="tab_ul set_up">
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> How to create an account</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> Account details</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> How to change username and password</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> I forgot the username</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> Account removal</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
					</ul>

					<ul class="tab_ul buy">
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> How to purchase</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> What`s the price</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> How much VAT is there</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> Transaction details</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> Cancelling the order</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
					</ul>

					<ul class="tab_ul pay">
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> How to get payed</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> Cash or credits</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> How to receive the money</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> Penalties</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> Delivery estimations</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
					</ul>

					<ul class="tab_ul card">
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> How to use the credit card</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> Operation details</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> Which banks</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> I forgot the SWIFT code</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
						<li>
							<h4 class="ascuns"><i class="fa fa-angle-down"></i> No credit card</h4>
							<p class="text_faq">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus Terry Richardson ad squid. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quae, fuga, nesciunt, pariatur commodi reiciendis saepe tempora sit perspiciatis deleniti totam corporis voluptate a doloremque nostrum!</p>
						</li>
					</ul>

					<h3>Couldn`t find what you are looking for?</h3>
					<div class="red_line_faq_options"></div>
					<p class="text_find">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates, molestiae, tempora, ab eaque unde tempore officia est dignissimos pariatur labore ullam ipsum expedita illo cupiditate repellendus aliquam vero quisquam. Nam.</p>

					<div class="div_contact_us">Contact Us</div>

					<div class="clear"></div>

					<div class="col-md-6 div_hover_faq">
						<h3><i class="fa fa-bolt"></i> PREMIUM SUPPORT</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis maiores repudiandae, accusantium reici dolorum amet!</p>
						<div class="red_line_faq"></div>
					</div>

					<div class="col-md-6 div_hover_faq">
						<h3><i class="fa fa-grav"></i> CUSTOMIZABLE</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis maiores repudiandae, accusantium reici dolorum amet!</p>
						<div class="red_line_faq"></div>
					</div>
				</div>

				<div class="col-md-3 div_beyond padding_right0">
					<h3>Above and beyond</h3>
					<div class="red_line_beyond"></div>
					<p>Lorem ipsum dolor sit amet, consect adipisicing elit. Impedit, recusandae corrupti voluptas atque voluptatibust!</p>

					<h3>Categories</h3>
					<ul>
						<li><a href=""><i class="fa fa-angle-right"></i> Web Design</a></li>
						<li><a href=""><i class="fa fa-angle-right"></i> Software Engineering</a></li>
						<li><a href=""><i class="fa fa-angle-right"></i> Web Graphic</a></li>
						<li><a href=""><i class="fa fa-angle-right"></i> Web Programming</a></li>
						<li><a href=""><i class="fa fa-angle-right"></i> Software Design</a></li>
					</ul>

					<h3>Tags</h3>
					<p class="tags">
						<a target="blank" href="https://en.wordpress.com/" title="Wordpress.com">Wordpress</a>
						<a target="blank" href="https://www.concrete5.org/" title="Concrete5.org">Concrete5</a>
						<a target="blank" href="https://www.drupal.org/" title="Drupal.org">Drupal</a>
					</p>

					<p class="tags">
						<a target="blank" href="https://www.joomla.org/" title="Joomla.org">Joomla</a>
						<a target="blank" href="http://php.net/manual/en/intro-whatis.php" title="PHP.net">PHP</a>
						<a target="blank" href="https://html5test.com/" title="HTML5-test.com">HTML5</a>
						<a target="blank" href="https://css3test.com/" title="CSS3-test.com">CSS3</a>
						<a target="blank" href="https://jquery.com/" title="JQuery.com">JQuery</a>
					</p>

					<p class="tags">
						<a target="blank" href="https://www.java.com/en/" title="Java.com">Java</a>
						<a target="blank" href="https://www.ruby-lang.org/en/" title="Ruby.org">Ruby</a>
						<a target="blank" href="https://www.javascript.com/" title="Javascript.com">Javascript</a>
					</p>

					<h3>Get social</h3>
					<div class="red_line_beyond"></div>
					<div class="col-md-12 div_social padding0" style="margin-top: 10px;">
						<div class="col-md-1 div_social_icon padding0">
							<a target="blank" href="https://www.twitter.com/" title="Twitter.com">
								<i class="fa fa-twitter"></i>
							</a>
						</div>
						<div class="col-md-4 padding0">
							<a target="blank" href="https://www.twitter.com/" title="Twitter.com">
								<span class="span_hover">Follow us</span>
							</a>
						</div>
						<span class="span_right">455 followers</span>
					</div>

					<div class="col-md-12 div_social padding0">
						<div class="col-md-1 div_social_icon padding0">
							<a target="blank" href="https://www.facebook.com/" title="Facebook.com">
								<i class="fa fa-facebook-f"></i>
							</a>
						</div>
						<div class="col-md-4 padding0">
							<a target="blank" href="https://www.facebook.com/" title="Facebook.com">
								<span class="span_hover">Like us</span>
							</a>
						</div>
						<span class="span_right">505 likes</span>
					</div>

					<div class="col-md-12 div_social padding0">
						<div class="col-md-1 div_social_icon padding0">
							<a href="" title="Enter ur email">
								<i class="fa fa-envelope"></i>
							</a>
						</div>
						<div class="col-md-4 padding0">
							<a href="" title="Enter ur email">
								<span class="span_hover">Subscribe</span>
							</a>
						</div>
						<span class="span_right">250 subscribers</span>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="to_top">
			<i class="fa fa-thumbs-up" aria-hidden="true"></i>
			<br>
			<p>to Top?</p>
		</div>

		<?php 
			include('/elements/footer.php');
		?>

		<?php
			include('/elements/scripts.php');
		?>
	</body>

</html>