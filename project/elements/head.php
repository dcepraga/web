<head>
	<title>Palas</title>
	<meta charset="UTF-8">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/web/project/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/web/project/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/web/project/css/slick.css">
	<link rel="stylesheet" type="text/css" href="/web/project/css/project.css">
	<link rel="stylesheet" type="text/css" href="/web/project/css/project-responsive.css">
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>