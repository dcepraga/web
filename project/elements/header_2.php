<div class="container-fluid bckg_color2 padding_left0 padding_right0">
	<div class="container div_1200 padding0">
		<div class="logo">
			<img src="/web/project/img/logo-light.png">
		</div>

		<div class="menu">
			<ul>
				<li><a href="/web/project/index.php">Home</a></li>
				<li><a href="/web/project/about.php">About us</a></li>
				<li><a href="/web/project/portfolio.php">Portfolio</a></li>
				<li><a href="/web/project/services.php">Services</a></li>
				<li><a href="/web/project/contact.php">Contact</a></li>
				<li class="documentation">
					<button class="dropbtn">Documentation</button>
					<ul class="submenu">
						<li><a href="/web/project/faq.php">FAQ</a></li>
						<li><a href="/web/project/team.php">Team</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="div_mobile">
			<i class="fa fa-bars mobile_hamburger" aria-hidden="true"></i>

			<ul class="menu_mobile">
				<li><a href="/web/project/index.php">Home</a></li>
				<li><a href="/web/project/about.php">About us</a></li>
				<li><a href="/web/project/portfolio.php">Portfolio</a></li>
				<li><a href="/web/project/services.php">Services</a></li>
				<li><a href="/web/project/contact.php">Contact</a></li>
				<li class="documentation_mobile">
					Documentation <i class="fa fa-angle-right"></i>
					<ul class="submenu meniu_doc">
						<li><a href="/web/project/faq.php">FAQ</a></li>
						<li><a href="/web/project/team.php">Team</a></li>
					</ul>
				</li>
			</ul>
			<div class="clear"></div>
		</div>
		
	</div>
</div>