<div class="container-fluid bckg_color1 padding_left0 padding_right0">
	<div class="container div_1200 padding0">
		<div class="header1_left">
			<ul>
				<li><a href="/web/project/about.php" style="padding-left: 0;">About</a></li>
				<li><a href="/web/project/services.php">Features</a></li>
				<li><a href="/web/project/elements/...">Pricing</a></li>
				<li><a href="/web/project/elements/...">Terms</a></li>
			</ul>
		</div>

		<div class="header1_right">
			<ul>
				<li><a target="blank" href="https://www.twitter.com/" title="Twitter.com"><i class="fa fa-twitter"></i></a></li>
				<li><a target="blank" href="https://www.facebook.com/" title="Facebook.com"><i class="fa fa-facebook-f"></i></a></li>
				<li><a target="blank" href="https://www.instagram.com/" title="Instagram.com"><i class="fa fa-instagram"></i></a></li>
				<li><a target="blank" href="https://dribbble.com/" title="Dribbble.com"><i class="fa fa-dribbble"></i></a></li>
				<li><a target="blank" href="https://www.behance.net/" title="Behance.net" style="padding-right: 0;"><i class="fa fa-behance"></i></a></li>
			</ul>
		</div>
	</div>
</div>