<div class="container-fluid bckg_color4 padding0">
	<div class="container div_footer padding_right0 padding_left0">
		<div class="col-md-4 div_footer_container padding_left0">
			<h4>Palas</h4>
			<div class="red_line_footer"></div>

			<ul class="footer_list_1">
				<li><a href="/web/project/index.php" style="padding-left: 0;">Home</a></li>
				<li><a href="/web/project/about.php">About us</a></li>
				<li><a href="/web/project/portfolio.php">Portfolio</a></li>
				<li><a href="/web/project/services.php">Services</a></li>
				<li><a href="/web/project/contact.php">Contact</a></li>
				<li><a href="/web/project/faq.php">FAQ</a></li>
				<li><a href="/web/project/team.php">Team</a></li>
			</ul>
		</div>

		<div class="col-md-4 div_footer_container bckg_img3">
			<h4>Get social</h4>
			<div class="red_line_footer"></div>

			<ul class="footer_list_2" style="display: block; padding-top: 1%">
				<li><a target="blank" href="https://www.twitter.com/" title="Twitter.com" style="padding-left: 0;"><i class="fa fa-twitter"></i></a></li>
				<li><a target="blank" href="https://www.facebook.com/" title="Facebook.com"><i class="fa fa-facebook-f"></i></a></li>
				<li><a target="blank" href="https://plus.google.com/discover" title="Google+.com"><i class="fa fa-google-plus"></i></a></li>
				<li><a target="blank" href="https://ro.pinterest.com/pinterest-international/" title="Pinterest.com"><i class="fa fa-pinterest"></i></a></li>
				<li><a target="blank" href="https://www.instagram.com/" title="Instagram.com"><i class="fa fa-instagram"></i></a></li>
				<li><a target="blank" href="https://dribbble.com/" title="Dribbble.com"><i class="fa fa-dribbble"></i></a></li>
			</ul>
		</div>

		<div class="col-md-4 div_footer_container padding_right0">
			<h4>Tweets</h4>
			<div class="red_line_footer"></div>

			<div class="col-md-1 div_twitter padding0">
				<i class="fa fa-twitter"></i>
			</div>
			<div class="col-md-11 div_tweet padding0">
				<p style="line-height: 220%;"><font color="red">@businesstheme</font> &nbsp; &nbsp; Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				<p><font color="silver" size="-1">22 hours ago</font></p>
			</div>
			<div class="clear"></div>
		</div>

		<div class="col-md-12 div_wrap padding0">
			<div class="col-md-4 div_copyright padding_left0">
				<p><i><i class="fa fa-copyright"></i> Copyright 2010 - 2015 abusinesstheme</i></p>
			</div>

			<div class="col-md-4 div_email">
				<input type="text" name="E-mail" class="form-control  Email" placeholder="Type email and hit Enter!">
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>