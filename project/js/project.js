$(document).ready(function(){

//================================================================================================== * Sectiune responsive

	$('.mobile_hamburger').click(function(){
		$('.menu_mobile').slideToggle();	
	});

	$('.documentation_mobile').click(function(){
		$('.meniu_doc').slideToggle();
	});
 
//================================================================================================= * Index(Homepage)

	var hgt = [];
		$('.div_height').each(function(){
		hgt.push($(this).outerHeight());
		});

	var max_h = Math.max(...hgt);
		$('.bckg_img2').css('height', max_h);


	if($(window).width() < 991)
		{
				$('.div_test').css('height', 'auto');
		}
	else
		{
			var cite = [];
				$('.div_cite_test').each(function(){
					cite.push($(this).outerHeight());
				});

			var test = Math.max(...cite);
				$('.div_test').css('height', test);
		}

//================================================================================================= * Slidere

	$('.header_slider').slick({
		dots: true,
  		infinite: true,
  		speed: 300,
  		slidesToShow: 1,
  		arrows: false,
		autoplay: true,
  		autoplaySpeed: 5500
	});

	if($(window).width() > 768)
		{
			$('.div_slider').slick({
				dots: false,
		  		infinite: true,
		  		speed: 300,
		  		slidesToShow: 5,
		  		arrows: true
			});
		}
	else
		{
			$('.div_slider').slick({
				dots: false,
		  		infinite: true,
		  		speed: 300,
		  		slidesToShow: 1,
		  		arrows: false
			});
		}

	$('.slick-next').html('<i class="fa fa-angle-right"></i>');
	$('.slick-prev').html('<i class="fa fa-angle-left"></i>');
	$('.slick-dots li').html('<i class="fa fa-circle"></i>');

//================================================================================================ * Div iconite stanga fixat pe body

	$('.news').click(function(){
		if($(this).hasClass('active'))
			{
				$(this).removeClass('active');
				$('.news').css('border-bottom-right-radius', '5px');
				$('.shou').fadeOut();
			}

		else
			{
				$(this).addClass('active');
				$('.news').css('border-bottom-right-radius', '0px');
				$('.shou').fadeIn();
			}
		
	});

	$(document).mouseup(function(e)	{
	    var container = $(".shou, .news");

	    // if the target of the click isn't the container nor a descendant of the container
	    if (!container.is(e.target) && container.has(e.target).length === 0)
			{
	        	$('.shou').fadeOut();
				$('.news').css('border-bottom-right-radius', '5px');
				$('.news').removeClass('active');
				e.preventDefault();
	    	}
	});

//=============================================================================================== * Div scroll to top

	$(window).scroll(function() {
    	if ($(this).scrollTop() > 200)	// this refers to window
			{ 
     			$('.to_top').fadeIn();
    		}
		else
			{
				$('.to_top').fadeOut();
			}
	});

	$('.to_top').click(function(){
		$("html, body").animate({
			scrollTop: $('.bckg_color1').offset().top
		}, 1500);
	});

//=============================================================================================== * Formular Contact page

	$('.button_submit').click(function(){
		var prenume = $('.first_name').val();
		var nume = $('.last_name').val();
		var subiect = $('.subject').val();
		var email = $('.e_mail').val();
		var mesaj = $('.message_contact').val();

		if(prenume != '' && nume != "" && subiect != "" && email != "" && mesaj != "")
			{
				$.ajax({
					url:"/fisierul_meu.php",
					data: 
						{
							prenume : prenume,
							nume : nume,
							subiect : subiect,
							email : email,
							mesaj : mesaj
						},
					method: 'POST',
					success: function(data)
						{
							//Aceste informatii se scriu pe success, dar nu putem evidentia acum.(Sunt trecute pe error pt. a testa!)
							$('.prenume').val("");
							$('.nume').val("");
							$('.subiect').val("");
							$('.email').val("");;
							$('.mesaj').val("");
						},
					error: function(jqXHR, textStatus, errorThrown)
						{
							alert('Error: '+jqXHR.status);
						}
				});
			}

		else
			{
				alert('Informatiile introduse nu sunt corecte!');
			}

	});

//=========================================================================================== * Pagina FAQ, sageti + taburi

	$('.ascuns').on('click', function(){
		$(this).parent().find('.text_faq').slideToggle();
		$(this).find($(".fa")).toggleClass('fa-angle-up fa-angle-down');
	});

	$('.tab_click').on('click', function(){

		$('.tab_ul').hide();
		$('.tab_click').removeClass('red');
		$(this).addClass('red');
		var x = $(this).attr('data-value');
		$('.'+x).show();
	});

//========================================================================================== * Pagina Team, marire poze

	$('.fa-times').click(function(){
		$('.div_pop_up').hide();
	});

	$('.div_team_pic').on('click', function(){
		var x = $(this).attr('data-value');
		$('.'+x).show();
	});

//========================================================================================== * Pagina About us

	if($(window).width() < 991)
		{
			$('.div_laptop').css('height', 'auto');
		}
	else
		{
			var motto = [];
			$('.mobile_motto').each(function(){
			motto.push($(this).innerHeight());
		});

			var laptop = Math.max(...motto);
			$('.div_laptop').css('height', laptop);
		}

	$('.about').on('click', function(){
		$(this).parent().find('.text_about').slideToggle();
		$(this).find($(".fa")).toggleClass('fa-angle-up fa-angle-down');
	});

});