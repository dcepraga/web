<!DOCTYPE html>

<html>

	<?php
		include('/elements/head.php');
	?>

	<body>

		<?php
			include('/elements/header_1.php');
		?>

		<div class="news" title="Socialize">
			<i class="fa fa-cogs"></i>

			<div class="FB shou" title="Facebook.com">
				<a target="blank" href="https://www.facebook.com/">
					<i class="fa fa-facebook-square" aria-hidden="true"></i>
				</a>
			</div>
			
			<div class="TW shou" title="Twitter.com">
				<a target="blank" href="https://www.twitter.com/">
					<i class="fa fa-twitter-square" aria-hidden="true"></i>
				</a>
			</div>

			<div class="LI shou" title="LinkedIn.com">
				<a target="blank" href="https://ro.linkedin.com/">
					<i class="fa fa-linkedin-square" aria-hidden="true"></i>
				</a>
			</div>
		</div>

		<div class="container-fluid bckg_img_404 padding0">
			<div class="div_rgba"></div>

			<?php
				include('/elements/header_2.php');
			?>

			<div class="container mobile_404 padding_left0 padding_right0">
					<h1>404 Not Found</h1>

				<div class="clear"></div>

				<input type="text" name="search" placeholder="Search and hit enter">
			</div>
		</div>

		<div class="container-fluid div_relative padding0">
			<div class="div_purchase"><b>PURCHASE</b></div>

			<div class="container div_404 padding_left0 padding_right0">
				<span><b>404</b></span>
				<p class="text_404"><b>Oops, This Page Could Not Be Found!</b></p>
				<p>Unfortunately the page you were looking for could not be found. It may be temporarily unavailable, moved or no longer exist. Check the URL you entered for any<br>mistakes and try again. Alternatively, you could take a look around the rest of our site.</p>
				<input type="button" name="butzon" value="GO HOME">
			</div>
		</div>

		<div class="to_top">
			<i class="fa fa-thumbs-up" aria-hidden="true"></i>
			<br>
			<p>to Top?</p>
		</div>

		<?php 
			include('/elements/footer.php');
		?>
	
		<?php
			include('/elements/scripts.php');
		?>
	</body>

</html>