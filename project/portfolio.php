<!DOCTYPE html>

<html>

	<?php
		include('/elements/head.php');
	?>

	<body>

		<?php
			include('/elements/header_1.php');
		?>

		<div class="news" title="Socialize">
			<i class="fa fa-cogs"></i>

			<div class="FB shou" title="Facebook.com">
				<a target="blank" href="https://www.facebook.com/">
					<i class="fa fa-facebook-square" aria-hidden="true"></i>
				</a>
			</div>
			
			<div class="TW shou" title="Twitter.com">
				<a target="blank" href="https://www.twitter.com/">
					<i class="fa fa-twitter-square" aria-hidden="true"></i>
				</a>
			</div>

			<div class="LI shou" title="LinkedIn.com">
				<a target="blank" href="https://ro.linkedin.com/">
					<i class="fa fa-linkedin-square" aria-hidden="true"></i>
				</a>
			</div>
		</div>

		<div class="container-fluid bckg_img_portfolio">
			<div class="div_rgba"></div>

			<?php
				include('/elements/header_2.php');
			?>

			<div class="container div_1200 padding_left0 padding_right0">
				<div class="col-md-5 div_portfolio padding0">
					<h1>Portfolio 2 Columns</h1>
					<div class="red_line_services"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum amet!</p>
				</div>

				<div class="clear"></div>

				<div class="div_portfolio_links">
					<ul>
						<li><a href=""><i class="fa fa-bars"></i></a></li>
						<li><a href="">Above</a></li>
						<li><a href="">Beyond</a></li>
						<li><a href="">Nowhere</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="container-fluid div_relative padding0">

			<div class="div_purchase_portfolio"><b>PURCHASE</b></div>

			<div class="container mobile_portfolio_pics padding_top_bottom padding_left0 padding_right0">
				<div class="div_portfolio_pics">
					<img src="/web/project/img/p06.jpg">
				</div>

				<div class="div_portfolio_pics" style="margin-right: 0;">
					<img src="/web/project/img/p01.jpg">
				</div>

				<div class="div_portfolio_pics">
					<img src="/web/project/img/p11.jpg">
				</div>

				<div class="div_portfolio_pics" style="margin-right: 0;">
					<img src="/web/project/img/p03.jpg">
				</div>

				<div class="div_portfolio_pics" style="margin-bottom: 0;">
					<img src="/web/project/img/p04.jpg">
				</div>

				<div class="div_portfolio_pics" style="margin-right: 0; margin-bottom: 0;">
					<img src="/web/project/img/p05.jpg">
				</div>

				<div class="clear"></div>
			</div>
		</div>

		<div class="to_top">
			<i class="fa fa-thumbs-up" aria-hidden="true"></i>
			<br>
			<p>to Top?</p>
		</div>

		<?php 
			include('/elements/footer.php');
		?>
	
		<?php
			include('/elements/scripts.php');
		?>
	</body>

</html>