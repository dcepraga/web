<!DOCTYPE html>

<html>

	<head>
		<meta charset="UTF-8">
		<link href="https://fonts.googleapis.com/css?family=Supermercado+One" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="/web/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/web/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="/web/css/Coleman.css">
		<link rel="stylesheet" type="text/css" href="/web/css/Colemanresponsive.css">
		<title> Coleman </title>
	</head>

	<body>

		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3 col-md-3 col-lg-3 col-xs-3 fixed no_padding">
					<div class="menu">HOME</div>
					<div class="menu">ABOUT</div>
					<div class="menu">RESUME</div>
					<div class="menu">PORTFOLIO</div>
					<div class="menu">BLOG</div>
					<div class="menu">CONTACT</div>
					<div class="menu">EXTRA</div>
				</div>

				<div class="col-sm-offset-3 col-md-offset-3 col-lg-offset-3 col-xs-offset-3
							col-sm-9 col-md-9 col-lg-9 col-xs-9 no_padding">

					<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 background1">
						<div class="col-md-offset-1 col-md-11 coleman">
							<h1>BRADON COLEMAN</h1>
							<p>Creative Designer</p>
							<div class="portfolio">VIEW PORTFOLIO</div>
							<div class="portfolio">FULL RESUME</div>
						</div>
						<div class="down"><i class="fa fa-angle-down shou" aria-hidden="true"></i><i class="fa fa-angle-up hyde" aria-hidden="true"></i></div>
					</div>

					<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 background2">
						<div class="col-sm-10 col-md-10 col-lg-10 col-xs-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-xs-offset-1">
							<div class="col-sm-3 col-md-3 col-lg-3 col-xs-3 statistics">
								<p class="text-center numbers">387</p><p class="text-center">Creative Ideas</p>
							</div>
							
							<div class="col-sm-3 col-md-3 col-lg-3 col-xs-3 statistics">
								<p class="text-center numbers">218</p><p class="text-center">Projects Live</p>
							</div>
							
							<div class="col-sm-3 col-md-3 col-lg-3 col-xs-3 statistics">
								<p class="text-center numbers">35</p><p class="text-center">Awards winners</p>
							</div>
							
							<div class="col-sm-3 col-md-3 col-lg-3 col-xs-3 statistics">
								<p class="text-center numbers">156</p><p class="text-center">Happy Customers</p>
							</div>
						</div>
					</div>
					
					<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 background3">
						<div class="col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-xs-offset-1
								    col-sm-5 col-md-5 col-lg-5 col-xs-5 Brandon">
							<img src="/web/img/boy.jpg">
						</div>

						<div class="col-sm-5 col-md-5 col-lg-5 col-xs-5 about_me">
							<h1>ABOUT ME</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus error assumenda architecto quasi,
							   dicta magnam eveniet perspiciatis facere voluptatibus magni atque quae totam impedit asperiores vero.</p>
							<div class="icons">
								<a target="blank" href="https://www.facebook.com/" title="Facebook"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
								<a target="blank" href="https://twitter.com/" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
								<a target="blank" href="https://www.instagram.com/" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
								<a target="blank" href="https://www.pinterest.com/" title="Pinterest"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
								<a target="blank" href="https://www.behance.net/" title="Behance"><i class="fa fa-behance" aria-hidden="true"></i></a>
								<a target="blank" href="https://dribbble.com/" title="Dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>

					<div class="col-md-12 div_skills_padding">
						<div class="col-md-offset-1 col-md-10 div_skills_header"><h1>MY SKILLS</h1></div>
						<div class="col-md-offset-1 col-md-10">
							<div class="col-md-4 div_skills">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus optio odio, culpa <b>assumenda laboriosam</b> omnis similique, dolorem voluptatem expedita perferendis delenti, unde voluptatum quam! Lorem ipsum!</p>
							</div>

							<div class="col-md-4 div_graphics">
								<div class="col-md-12 container_percentages">
									<div class="domain">Webdesign</div>
									<div class="percentage_85">85%</div>
									<div class="clear"></div>
									<div class="div_gradient85"></div>
									<div class="div_remainder15"></div>
								</div>
								<div class="col-md-12 container_percentages">
									<div class="domain">Branding</div>
									<div class="percentage_75">75%</div>
									<div class="clear"></div>
									<div class="div_gradient75"></div>
									<div class="div_remainder25"></div>
								</div>
								<div class="col-md-12 container_percentages">
									<div class="domain">Photography</div>
									<div class="percentage_95">95%</div>
									<div class="clear"></div>
									<div class="div_gradient95"></div>
									<div class="div_remainder5"></div>
								</div>
							</div>

							<div class="col-md-4 div_graphics">
								<div class="col-md-12 container_percentages">
									<div class="domain">Webdesign</div>
									<div class="percentage_85">85%</div>
									<div class="clear"></div>
									<div class="div_gradient85"></div>
									<div class="div_remainder15"></div>
								</div>
								<div class="col-md-12 container_percentages">
									<div class="domain">Branding</div>
									<div class="percentage_75">75%</div>
									<div class="clear"></div>
									<div class="div_gradient75"></div>
									<div class="div_remainder25"></div>
								</div>
								<div class="col-md-12 container_percentages">
									<div class="domain">Photography</div>
									<div class="percentage_95">95%</div>
									<div class="clear"></div>
									<div class="div_gradient95"></div>
									<div class="div_remainder5"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>

					<div class="col-md-12 div_services">
						<div class="col-md-offset-1 col-md-10 div_services_texts">
							<h1>MY SERVICES</h1>
							<p>Labores inciderint complectitur ei vim, in molestiae!</p>
						</div>

						<div class="col-md-offset-1 col-md-10 margin_top">
							<div class="container_services">
								<i class="fa fa-laptop"></i>
								<h4>Webdesign</h4>
								<p>Appetere moderatius nam an. No essent dissentias vel, an ludus munere mel. Persius gloriatur!</p>
							</div>

							<div class="container_services">
								<i class="fa fa-star"></i>
								<h4>Branding</h4>
								<p>Appetere moderatius nam an. No essent dissentias vel, an ludus munere mel. Persius gloriatur!</p>
							</div>

							<div class="container_services margin_right0">
								<i class="fa fa-camera"></i>
								<h4>Photography</h4>
								<p>Appetere moderatius nam an. No essent dissentias vel, an ludus munere mel. Persius gloriatur!</p>
							</div>

							
							<div class="container_services">
								<i class="fa fa-code"></i>
								<h4>Development</h4>
								<p>Appetere moderatius nam an. No essent dissentias vel, an ludus munere mel. Persius gloriatur!</p>
							</div>

							<div class="container_services">
								<i class="fa fa-cart-plus"></i>
								<h4>Marketing</h4>
								<p>Appetere moderatius nam an. No essent dissentias vel, an ludus munere mel. Persius gloriatur!</p>
							</div>

							<div class="container_services margin_right0">
								<i class="fa fa-youtube"></i>
								<h4>Cinema visits</h4>
								<p>Appetere moderatius nam an. No essent dissentias vel, an ludus munere mel. Persius gloriatur!</p>
							</div>
						</div>
						<div class="clear"></div>
					</div>

					<div class="col-md-12 div_vitae">
						<div class="col-md-offset-1 col-md-10">
							<h1>RESUME</h1>

							<div class="col-md-12 div_resume">
								<span><i class="fa fa-graduation-cap"></i> Education</span>
								<i class="fa fa-angle-down"></i>
							</div>

							<div class="col-md-12 div_resume">
								<span><i class="fa fa-user"></i> Work Experiences</span>
								<i class="fa fa-angle-down"></i>
							</div>

							<div class="col-md-12 div_resume">
								<span><i class="fas fa-trophy"></i> Awards, Honors and Certifications</span>
								<i class="fa fa-angle-up"></i>
							</div>

							<div class="div_download">DOWNLOAD FULL RESUME</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script type="text/javascript" src="/web/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/web/js/coleman.js"></script>
	</body>

</html>