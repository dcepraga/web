<!DOCTYPE html>

<html>

	<head>
		<meta charset="UTF-8">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700,800" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="/web/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/web/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="/web/css/slick.css">
		<link rel="stylesheet" type="text/css" href="/web/css/Travel.css">
		<title> Travel </title>
	</head>

	<body>

		<div class="Pop_up1">
			<div class="Popup_inner">
				<div>
					<i class="fa fa-times"></i>
					<div class="clear"></div>
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
				</div>
			</div>
		</div>

		<div class="Pop_up2">
			<div class="Popup_inner">
				<div>
					<i class="fa fa-times"></i>
					<div class="clear"></div>
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
				</div>
			</div>
		</div>

		<div class="Pop_up3">
			<div class="Popup_inner">
				<div>
					<i class="fa fa-times"></i>
					<div class="clear"></div>
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row div_relative">

				<div class="col-md-12 div_absolute">
					<div class="col-md-8 div_menu">

						<i class="fa fa-times-circle"></i>
						<div class="clear"></div>
						<div class="div_photo">
							<img src="/web/img/gurl.jpg">
						</div>

						<p class="text-center" style="font-size: 65px;"><font color="darkorange"><b>Jane Doe</b></font></p>
						<p class="text-center" style="font-size: 55px;"><font color="white"><b>Photographer</b></font></p>

						<ul class="list">
							<li><i class="fa fa-home"></i> <b>Home</b></li>
							<li class="second">
								<i class="fa fa-book"></i> <b>Pages</b>
								<i class="fa ascuns fa-sort-down"></i>
								<ul class="submenu">
									<li>Submenu 1</li>
									<li>Submenu 2</li>
									<li>Submenu 3</li>
								</ul>
							</li>
							<li><i class="fa fa-map"></i> <b>Destination</b></li>
							<li><i class="fa fa-android"></i> <b>Our Team</b></li>
							<li><i class="fa fa-cubes"></i> <b>Gallery</b></li>
							<li><i class="fa fa-rss"></i> <b>Blog</b></li>
							<li><i class="fa fa-envelope"></i> <b>Contact</b></li>
						</ul>
					</div>
				</div>
			
				<div class="col-md-12 div_header">
					<div class="plane">
						<font color="darkorange"><i class="fa fa-plane"></i></font>
					</div>
						<p><font color="white"><b>Travel</b></font></p>
						<font color="white"><i class="fa fa-bars"></i></font>
				</div>

				<div class="col-md-12 background_1">
					<div class="col-md-4 div_option border_right">
						<div class="hover">
							<i class="fa fa-user"></i>
							<p><font color="white"><b>ACCOUNT</b></font></p>
						</div>
					</div>

					<div class="col-md-4 div_option">
						<div class="hover">
							<i class="fa fa-ravelry"></i>
							<p><font color="white"><b>MY TRIP</b></font></p>
						</div>
					</div>

					<div class="col-md-4 div_option border_left">
						<div class="hover">
							<i class="fa fa-bullseye"></i>
							<p><font color="white"><b>FAVOURITE</b></font></p>
						</div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="col-md-12 div_slider">
					<div class="slider_x">
						<img src="/web/img/exotic.jpg">
						<div class="background_3">
							<p class="opaque"><font color="white">Plan The</font></p>
							<p><font color="white">Best Trip</font></p>
							<p class="opaque"><font color="white">Ever</font></p>
						</div>
					</div>

					<div class="slider_x">
						<img src="/web/img/Naga.jpg">
						<div class="background_3">
							<p class="opaque"><font color="white">Plan The</font></p>
							<p><font color="white">Best Trip</font></p>
							<p class="opaque"><font color="white">Ever</font></p>
						</div>
					</div>

					<div class="slider_x">
						<img src="/web/img/gurlies.jpg">
						<div class="background_3">
							<p class="opaque"><font color="white">Plan The</font></p>
							<p><font color="white">Best Trip</font></p>
							<p class="opaque"><font color="white">Ever</font></p>
						</div>
					</div>
					
					<div class="slider_x">
						<img src="/web/img/bicycle.jpg">
						<div class="background_3">
							<p class="opaque"><font color="white">Plan The</font></p>
							<p><font color="white">Best Trip</font></p>
							<p class="opaque"><font color="white">Ever</font></p>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<div class="col-md-12 div_service">
					<p class="text-center"><font color="darkorange"><b>Our</b></font> <font color="darkblue"><b>Service</b></font></p>
				</div>
			</div>

			<div class="col-md-12 div_chests">
				<div class="div_chest" data-value="Pop_up1">
					<font color="red"><i class="fa fa-check"></i></font>
					<p><b>Booking</b></p>
					<p><b>Tickets</b></p>
					<div class="div_img">
						<img src="/web/img/Leap.jpg">
					</div>
				</div>

				<div class="div_chest margins" data-value="Pop_up2">
					<font color="green"><i class="fa fa-grav"></i></font>
					<p><b>Planning</b></p>
					<p><b>Tours</b></p>
					<div class="div_img">
						<img src="/web/img/bikini.jpg">
					</div>
				</div>

				<div class="div_chest" data-value="Pop_up3">
					<font color="darkred"><i class="fa fa-id-card"></i></font>
					<p><b>Documents</b></p>
					<div class="div_img">
						<img src="/web/img/Bryce.jpg">
					</div>
				</div>

				<div class="clear"></div>

				<div class="div_chest" data-value="Pop_up1">
					<font color="darkorange"><i class="fa fa-umbrella"></i></font>
					<p><b>Travel</b></p>
					<p><b>Insurence</b></p>
					<div class="div_img">
						<img src="/web/img/cover.jpg">
					</div>
				</div>

				<div class="div_chest margins" data-value="Pop_up1">
					<font color="lightblue"><i class="fa fa-building"></i></font>
					<p><b>Booking</b></p>
					<p><b>Hotel</b></p>
					<div class="div_img">
						<img src="/web/img/bicycle.jpg">
					</div>
				</div>

				<div class="div_chest" data-value="Pop_up1">
					<font color="blue"><i class="fa fa-car"></i></font>
					<p><b>Transport</b></p>
					<div class="div_img">
						<img src="/web/img/McKinley.jpg">
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script type="text/javascript" src="/web/js/slick.min.js"></script>
		<script type="text/javascript" src="/web/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/web/js/Travel.js"></script>
	</body>

</html>