<div class="container-fluid footer_background">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="div_logo"><strong>SIMPLESITE</strong></div>
				<div class="clear red_line2"></div>
				<div class="div_text">
					Our user experience ideals run through<br>every vein of our company. Our ideals are<br>not to attain perfection, but rather to attain<br>pleasure. Using our product should feel<br>enjoyable - like playing a game - it should<br>be easy, exciting and uplifting. Something<br>to try and subsequently to learn from. This<br>is the same experience our employees<br>have working for us.
				</div>
				<div class="div_icons">
					<a target="_blank" href="https://www.facebook.com/" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a target="_blank" href="https://www.youtube.com/" title="Youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
					<a target="_blank" href="https://twitter.com/" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a target="_blank" href="https://www.linkedin.com/" title="LinkedIn"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				</div>
			</div>

			<div class="col-lg-3">
				<div class="col-lg-9 col-lg-offset-3">
					<div class="div_logo"><strong>PRODUCT</strong></div>
					<div class="clear red_line2"></div>
					<div class="div_span">
						<span class="span_hover">Features</span>
						<span class="span_hover">Themes</span>
						<span class="span_hover">Gift Certificate</span>
					</div>
				</div>
			</div>

			<div class="col-lg-3">
				<div class="col-lg-11 col-lg-offset-1">
					<div class="div_logo"><strong>COMPANY</strong></div>
					<div class="clear red_line2"></div>
					<div class="div_span">
						<span class="span_hover">Team</span>
						<span class="span_hover">Nordic Growth Hackers</span>
						<span class="span_hover">Partnership</span>
						<span class="span_hover">Jobs</span>
						<span class="span_hover">Terms of use</span>
						<span class="span_hover">Privacy Policy</span>
					</div>
				</div>
			</div>

			<div class="col-lg-3 div_padding">
				<div class="div_logo"><strong>COMMUNITY</strong></div>
				<div class="clear red_line2"></div>
				<div class="div_span">
					<span class="span_hover">Customer service</span>
					<span class="span_hover">Our blog</span>
					<span class="span_hover">FAQ</span>
				</div>
			</div>

		</div>
	</div>
</div>