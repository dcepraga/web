$(document).ready(function(){
	$('.elements span').css({"color": "red" , "font-size": "15px"});
	//$('span').parent().css("cssText", "color: red; font-size: 15px;");
	setTimeout(function(){
		alert("Aceasta este o alerta! FUGI!");
	},3000);
	var text = $('.elements').text();
	$('.elements').append("Text: " + text);
	console.log(text);

	$('.add_class').click(function(){
		$('.something').addClass('something_else');
	});

	$('.remove_class').click(function(){
		$('.something').removeClass('something_else');
	});

	$('.lista').addClass('lista_activa');
	$('.second_li').prev().css({"color": "red"});
	$('.second_li').nextAll().css({"color": "green"});

	$('#btnAnimate').click(function(){
		setTimeout(function(){
			$('#animateMe').animate({
				'left': '300px',
				'top': '200px',
				'border-width': '8px',
				'color': 'red'
			}, 2000);
		}, 2000);
	});

	$('#btnAnimateBack').click(function(){
		$('#animateMe').animate({
			'left': '0',
			'top': '0',
			'border-width': '1px'
		}, 2000);
	});

	$('.accordion_header').css({"cursor": "pointer"});

	$('.accordion_header').click(function(){
		/*$('.accordion_content').slideToggle();****
		$('.accordion_content').removeClass('clicked');
		$('.accordion_content').hide();
		$(this).next().slideToggle();
		$(this).next().addClass('clicked');*/

		if ($(this).hasClass('clicked'))
			{
				$('.accordion_header').removeClass('clicked');
				$('.accordion_content').slideUp();
			}
		else
			{
				$('.accordion_header').removeClass('clicked');
				$('.accordion_content').hide();
				$(this).toggleClass('clicked');
				$(this).next().slideToggle();
			}
	});

});

var hgt = [];
	$('.tall').each(function(){
		hgt.push($(this).height());
	});
var max_h = Math.max(...hgt);
	$('.tall').css('height', max_h);
	console.log(max_h);

