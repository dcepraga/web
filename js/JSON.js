$(document).ready(function(){

	var array_products = [];

	$.getJSON( "/web/products.json", function( data ) {

		$.each( data, function( key, val ) {

			$.each( val, function( key2, val2 ) {
				array_products.push(val2);
				console.log(val2);

				var res = val2.body_html.substr(0, 200);

				$('.parent1').append('<div class="parinte_produs"><div class="titlu_produs sss">'+val2.title+'</div>'+
				'<div class="descriere_produs sss"><div class="descriere_scurta">'+res+'</div><div class="descriere_lunga">'+val2.body_html+
				'</div><div class="citeste_mai_mult">Citeste mai mult</div><div class="citeste_mai_putin">Citeste mai putin</div></div>'+
				'<div class="link_produs sss"><a href="'+val2.handle+'">Link</a></div>'+'<div class="imagine_produs sss"><img src="'+
				val2.images[0].src+'"></div><div class="clear"></div></div>');
			});
		});
	});

	$('body').on('click','.citeste_mai_mult',function(){
		$(this).parent().find('.descriere_scurta').hide();
		$(this).parent().find('.descriere_lunga').fadeIn();
		$(this).parent().find('.citeste_mai_mult').hide();
		$(this).parent().find('.citeste_mai_putin').show()
	});

	$('body').on('click','.citeste_mai_putin',function(){
		$(this).parent().find('.descriere_scurta').show();
		$(this).parent().find('.descriere_lunga').fadeOut();
		$(this).parent().find('.citeste_mai_putin').hide();
		$(this).parent().find('.citeste_mai_mult').show()
	});
});