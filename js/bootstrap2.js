$('.project').slick({
		dots: true,
  		infinite: true,
  		speed: 300,
  		slidesToShow: 1,
  		arrows: true
	});

	$('.slick-next').html('<i class="fa fa-angle-right"></i>');
	$('.slick-prev').html('<i class="fa fa-angle-left"></i>');