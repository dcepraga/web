$(document).ready(function(){
	$('.fa-bars').click(function(){
		$('.div_absolute').fadeIn();
	});

	$('.fa-times-circle').click(function(){
		$('.div_absolute').fadeOut();
	});


	$('.fa-sort-down').on('click', function(){
		$(this).parent().find('.submenu').slideToggle();
		$('.ascuns').toggleClass('fa-sort-down fa-sort-up');
	});

	$('.div_chest').click(function(){
		//$('.div_img').hide();
		//$(this).find('.div_img').fadeIn(1000);

		var x = $(this).attr('data-value');
		//alert(x);
		$('.'+x).fadeIn();
	});

	$('.fa-times').click(function(){
		$('.Pop_up1').fadeOut();
		$('.Pop_up2').fadeOut();
		$('.Pop_up3').fadeOut();
	});

	$('.div_img').click(function(){
		$('.div_img').fadeOut();
		$(this).parent('.div_chest').fadeIn(1000);
	});

	$('.div_slider').slick({
		dots: true,
  		infinite: true,
  		speed: 300,
  		slidesToShow: 1,
  		arrows: true
	});

	$('.slick-next').html('<i class="fa fa-angle-right"></i>');
	$('.slick-prev').html('<i class="fa fa-angle-left"></i>');
	$('.slick-dots li').html('<i class="fa fa-circle"></i>');
});

var hgt = [];
	$('.div_chest').each(function(){
		hgt.push($(this).outerHeight());
	});

var max_h = Math.max(...hgt);
	$('.div_chest').css('height', max_h);