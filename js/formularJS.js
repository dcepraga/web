$(document).ready(function(){
	$('.trimite_formular').click(function(){
		var nume = $('.nume').val();
		var prenume = $('.prenume').val();
		var telefon = $('.telefon').val();
		var email = $('.email').val();
		var selecteaza_dep = $('.selecteaza_dep').val();
		var checkbox = $('.checkbox:checked').val();
		var mesaj = $('.mesaj').val();

		if(nume != '' && prenume != "" && telefon != "" && email != "" && mesaj != "")
			{
				$.ajax({
					url:"/fisierul_meu.php",
					data: 
						{
							nume : nume,
							prenume : prenume,
							telefon : telefon,
							email : email,
							selecteaza_dep : selecteaza_dep,
							checkbox : checkbox,
							mesaj : mesaj
						},
					method: 'POST',
					success: function(data)
						{
							alert('Success!');

							//Aceste informatii se scriu pe success, dar nu putem evidentia acum.(Sunt trecute pe error pt. a testa!)
							$('.nume').val("");
							$('.prenume').val("");
							$('.telefon').val("");
							$('.email').val("");
							$('.selecteaza_dep').val("departament info");
							$('.checkbox:checked').val("");
							$('.mesaj').val("");

							$('.formular').hide();
							$('.afiseaza_mesaj').fadeIn();
						},
					error: function(jqXHR, textStatus, errorThrown)
						{
							alert('Error: '+jqXHR.status);
						}
				});
			}
		else
			{
				alert('Va rugam completati toate campurile!');
			}
	});

	var array_products = [];
	$.getJSON("https://hancockseed-com.myshopify.com/products.json?limit=250&page=1", function(data){
		console.log(data);
		$.each(data, function(key, val){
			$.each(val, function(key2, val2){
				array_products.push(val2);
			});
		});
	});

});

//$_POST['nume:ex','prenume:prenume']