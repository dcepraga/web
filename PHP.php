<!DOCTYPE html>

<html>

	<body>

		<?php
			include('/elements/head.php');
		?>

		<div class="container-fluid background php_page">
			<div class="div_rgba"></div>
				<?php
					include('/elements/meniu.php');
				?>
			<div class="div_features"><p class="text-center">Features</p></div>
		</div>

		<div class="container-fluid">
			<div class="container">
				<div class="row">

					<div class="website_features">
						<span class="span_grey">YOUR WEBSITE</span>
						<span class="span_normal"><strong>FEATURES INCLUDE:</strong></span>
					</div>

					<div class="clear red_line"></div>

					<div class="long_span">
						<span class="text_grey">Check out all the</span>
						<span class="text_normal">great features</span>
						<span class="text_grey">that are included on your website! SimpleSite provides everything you need to create a beatiful and professional website.</span>
					</div>

					<div class="clear"></div>

					<div class="col-lg-4 div_all">
						<div class="div_awesome"><i class="fa fa-paint-brush" aria-hidden="true"></i></div>
						<div class="dark_text"><span>Design, backgrounds and colours</span></div>
						<p class="text-center">A SimpleSite website lets you choose between<br>hundreds of unique designs. Choose between our<br>many different templates, colours and backgrounds<br>- or choose a ready-made design.</p>
					</div>

					<div class="col-lg-4 div_all">
						<div class="div_awesome"><i class="fa fa-at" aria-hidden="true"></i></div>
						<div class="dark_text"><span>Personal Domain Name</span></div>
						<p class="text-center">You can choose your unique domain name. A<br>domain name will make your website more personal<br>and easier to find on search engines like Google.<br>Having your domain name will also help<br>strengthen your brand.</p>
					</div>

					<div class="col-lg-4 div_all">
						<div class="div_awesome"><i class="fa fa-cart-plus" aria-hidden="true"></i></div>
						<div class="dark_text"><span>Your own online store</span></div>
						<p class="text-center">Your SimpleSite website also includes your own<br>professional online store that enables you to offer<br>online payments. Regardless of whether you have<br>your own company or just want to sell a few things<br>now and then, our online store is perfect for you.</p>
					</div>

					<div class="col-lg-4 div_all">
						<div class="div_awesome"><i class="fa fa-comments-o" aria-hidden="true"></i></div>
						<div class="dark_text"><span>24/7 Customer Service</span></div>
						<p class="text-center">Our customer service is ready to help you when you<br>have questions - or check out our frequently asked<br>questions in our customer service section.</p>
					</div>

					<div class="col-lg-4 div_all">
						<div class="div_awesome"><i class="fa fa-camera" aria-hidden="true"></i></div>
						<div class="dark_text"><span>Photo Album</span></div>
						<p class="text-center">Showcase your photos in a beatiful online photo<br>album. Choose between various slideshow themes,<br>layouts and photo transitions.</p>
					</div>

					<div class="col-lg-4 div_all">
						<div class="div_awesome"><i class="fa fa-video-camera" aria-hidden="true"></i></div>
						<div class="dark_text"><span>Video</span></div>
						<p class="text-center">Make your website more dynamic by using our video<br>player. You can add videos from Youtube or Vimeo - <br>or upload your own videos.</p>
					</div>

					<div class="col-lg-4 div_all">
						<div class="div_awesome"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></div>
						<div class="dark_text"><span>Contact Page</span></div>
						<p class="text-center">The Contact Page make it easy for your visitors to<br>send private messages directly to you.</p>
					</div>

					<div class="col-lg-4 div_all">
						<div class="div_awesome"><i class="fa fa-map-o" aria-hidden="true"></i></div>
						<div class="dark_text"><span>Map</span></div>
						<p class="text-center">Add a map on your website to show visitors where<br>you are located.</p>
					</div>

					<div class="col-lg-4 div_all">
						<div class="div_awesome"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
						<div class="dark_text"><span>Email addresses</span></div>
						<p class="text-center">Included with each domain purchase are 5 personal<br>email addresses.<br>You could for example choose.<br>yourfirstname@domainname.com</p>
					</div>

					<div class="col-lg-4 div_allq col-lg-offset-2">
						<div class="div_awesome"><i class="fa fa-thumbs-up" aria-hidden="true"></i></div>
						<div class="dark_text"><span>Works on all devices</span></div>
						<p class="text-center">Your website has been optimised for viewing on PC`s,<br>tablets and mobile phones.</p>
					</div>

					<div class="col-lg-4 div_allq">
						<div class="div_awesome"><i class="fa fa-arrows" aria-hidden="true"></i></div>
						<div class="dark_text"><span>Social media sharing</span></div>
						<p class="text-center">Your visitors can "Like" you website and share it on<br>their own Facebook, Twitter or Google+ profiles.</p>
					</div>

				</div>
			</div>
		</div>

		<?php
			include('/elements/footer.php');
		?>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script type="text/javascript" src="/web/js/bootstrap.min.js"></script>
	</body>

</html>