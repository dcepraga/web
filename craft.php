<!DOCTYPE html>

<html>

	<head>
		<meta charset="UTF-8">
		<link href="https://fonts.googleapis.com/css?family=Risque:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="/web/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/web/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="/web/css/craft.css">
		<link rel="stylesheet" type="text/css" href="/web/css/craft-responsive.css">
		<title> Craftbeer </title>
	</head>

	<body>
		
		<div class="container-fluid">
			<div class="row">
			
				<div class="col-sm-2 col-md-2 col-lg-2 col-xs-2 background1">

					<div class="craftbeer">
						<img src="/web/img/Bottle-Cap.png">
					</div>

					<div class="logo">
						<img src="/web/img/rare-bird-craft-beer-logo.png">
					</div>

					<div class="menu">
						<ul class="menu_list">
							<li><a href="">Home</a></li>
							<li><a href="">Our Brewery</a></li>
							<li><a href="">Beers</a></li>
							<li><a href="">How it`s made</a></li>
							<li><a href="">Contact</a></li>
							<li><a href="">Buy Now</a></li>
						</ul>				
					</div>

				</div>

				<div class="col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xs-offset-2
							col-sm-10 col-md-10 col-lg-10 col-xs-10 background2">

					<p class="text-center">1st class natural fresh local crafted beer</p>
					
					<div class="wood_planks">
						<img src="/web/img/home_craftbeer_slider_bg2.jpg">
						<div class="bottle">
							<img src="/web/img/home_craftbeer_slider_1.png">
						</div>
					</div>

				</div>

				<div class="col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xs-offset-2
							col-sm-10 col-md-10 col-lg-10 col-xs-10 background3">
				
					<p class="text-center art">We respect the traditional art of brewing and craft <span class="white">one of a kind</span>
											   beer for real gourments</p>

					<div class="col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xs-offset-2
								col-sm-4 col-md-4 col-lg-4 col-xs-4 margin">
						<p class="brewery">Brewery with tradition</p>
				
						<div class="cellar">
							<img src="/web/img/buenavista.jpg">
						</div>

						<p class="mauris">Mauris rhoncus orci in imperdiet placerat. Vestibulum euismod nisl suscipit ligula volutpat, a 
										  feugiat urna maximus. Cras massa nibh, tincidunt ut eros a, vulputate consequat odio. Vestibulum vehicula tempor nulla, sed hendrerit urna interdum in. Donec et nibh maximus, congue est eu, mattis nunc. Praesent ut quam quis quam venenatis fringilla vestibulum.
										  </p>

						<div class="read_more">
							<p class="text-center">Read more</p>
						</div>

						<div class="div_arrow">
							<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
						</div>

					</div>

					<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4">
						<div class="beer">
							<img src="/web/img/home_craftbeer_contentslider2.png">
						</div>
							
						<div class="div_icons">
							<i class="fa fa-home" aria-hidden="true"></i>
							<i class="fa fa-circle" aria-hidden="true"></i>
						</div>

						<p class="text-center mauris">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mauris dolor, gravida a varius blandit, auctor eget purus. Phasellus scelerisque sapien sit.</p>
				
						<div class="discover">
							<p class="text-center">Discover all flavours</p>
						</div>

						<div class="div_arrow">
							<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
						</div>
				
					</div>
				
					<div class="symbol">
						<img src="/web/img/home_craftbeer_pic2.png">
					</div>		
				
				</div>

				<div class="col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xs-offset-2
							col-sm-10 col-md-10 col-lg-10 col-xs-10 background4 margin_top1">
					<p class="text-center mauris">Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non 
												  felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a convallis ac, laoreet enim. Phasellus fermentum in, dolor.</p>
				</div>

				<div class="col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xs-offset-2
							col-sm-10 col-md-10 col-lg-10 col-xs-10 background5 margin_top2">
					<p class="text-center mauris"><i class="fa fa-copyright" aria-hidden="true"></i> 2018 BeCraftBeer - BeTheme. All Rights Reserved. Muffin Group</p>
				</div>
			
			</div>
		</div>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script type="text/javascript" src="/web/js/bootstrap.min.js"></script>
	</body>

</html>