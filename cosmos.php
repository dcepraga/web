<!DOCTYPE html>

<html>

	<head>
		<meta charset="UTF-8">
		<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="/web/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="/web/css/cosmos.css">
		<link rel="stylesheet" type="text/css" href="/web/css/cosmosresponsive.css">
		<title> Cosmos </title>
	</head>

	<body>

		<div class="first_page">

			<div class="header">
				<div class="header_left animation_tone">Onetone</div>
				<div class="header_right animation_menu">
					<ul class="menu">
						<li><a href="">HOME</a></li>
						<li><a href="">SERVICES</a></li>
						<li><a href="">GALLERY</a></li>
						<li><a href="">TEAM</a></li>
						<li><a href="">ABOUT</a></li>
						<li><a href="">TESTIMONIALS</a></li>
						<li><a href="">CONTACT</a></li>
						<li><a href="">BLOG</a></li>
						<li><a href="">GO SLIDER DEMO</a></li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>

			<div class="middle_content">
				<p class="powerfull animation_powerfull">POWERFULL ONE PAGE THEME</p>

				<p class="text_middle animation_txtmiddle">BASED ON BOOTSTRAP FRAMEWORK AND SHORTCODES, QUICK SET AND EASY BUILD, SHINES ONE PAGE SMALL<br>BUSINESS WEBSITE.</p>
				<div class="clear"></div>
				<div class="click_me animation_click">Click Me</div>

				<div class="links animation_links">
					<a target="blank" href="https://www.facebook.com/" title="facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a target="blank" href="https://www.skype.com/en/" title="skype.com"><i class="fa fa-skype" aria-hidden="true"></i></a>
					<a target="blank" href="https://twitter.com/" title="twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a target="blank" href="https://www.linkedin.com/" title="linkedin.com"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					<a target="blank" href="https://plus.google.com/" title="google-plus.com"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
					<a target="blank" href="https://feed.com/#" title="feed.com"><i class="fa fa-rss" aria-hidden="true"></i></a>
				</div>
			</div>

			<div class="play">
				<i class="fa fa-pause" aria-hidden="true"></i>
				<i class="fa fa-volume-up" aria-hidden="true"></i>
			</div>
		</div>

		<div class="services">
			<span class="site_general"><b>OUR SERVICES</b></span>
			<p class="latin">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere c. Etiam ut dui eu nisi lobortis rhoncus 
							 ac quis nunc.</p>

			<div class="services_middle">
				<div class="container_services margin_right">
					<div class="mini_png">
						<img src='/web/img/Double_Decker_Bus.png'>
					</div>
					<p class="text_large"><b>FLEXIBLE DESIGN</b></p>
					<p class="text_small">Integer pulvinar elementum est suscipit ornare ante finibus ac. Praesent vel ex dignissim rhoncus eros luctus dignissim arcu.</p>
				</div>

				<div class="container_services">
					<div class="mini_png">
						<img src='/web/img/palmtree.png'>
					</div>
					<p class="text_large"><b>SHORTCODES GENERATOR</b></p>
					<p class="text_small">Integer pulvinar elementum est suscipit ornare ante finibus ac. Praesent vel ex dignissim rhoncus eros luctus dignissim arcu.</p>
				</div>

				<div class="container_services margin_left">
					<div class="mini_png">
						<img src='/web/img/Sydney.png'>
					</div>
					<p class="text_large"><b>FREE PSD TEMPLATE</b></p>
					<p class="text_small">Integer pulvinar elementum est suscipit ornare ante finibus ac. Praesent vel ex dignissim rhoncus eros luctus dignissim arcu.</p>
				</div>

				<div class="container_services margin_right">
					<div class="mini_png">
						<img src='/web/img/Indian_tent.png'>
					</div>
					<p class="text_large"><b>FREE DEMO CONTENTS</b></p>
					<p class="text_small">Integer pulvinar elementum est suscipit ornare ante finibus ac. Praesent vel ex dignissim rhoncus eros luctus dignissim arcu.</p>
				</div>

				<div class="container_services">
					<div class="mini_png">
						<img src='/web/img/House.png'>
					</div>
					<p class="text_large"><b>SEO OPTIMIZATION</b></p>
					<p class="text_small">Integer pulvinar elementum est suscipit ornare ante finibus ac. Praesent vel ex dignissim rhoncus eros luctus dignissim arcu.</p>
				</div>

				<div class="container_services margin_left">
					<div class="mini_png">
						<img src='/web/img/Signs.png'>
					</div>
					<p class="text_large"><b>CONTINUOUS SUPPORT</b></p>
					<p class="text_small">Integer pulvinar elementum est suscipit ornare ante finibus ac. Praesent vel ex dignissim rhoncus eros luctus dignissim arcu.</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="div_gallery">
			<span class="site_general"><b>GALLERY</b></span>
			<p class="latin">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere c. Etiam ut dui eu nisi lobortis rhoncus 
							 ac quis nunc.</p>
			<div class="gallery_pics">
				<img src="/web/img/web.jpg" alt="Image">
				<img src="/web/img/working.jpg" alt="Image">
				<img src="/web/img/working2.jpg" alt="Image">
				<img src="/web/img/project.jpg" alt="Image">
				<img src="/web/img/inspiring.jpg" alt="Image">
				<img src="/web/img/Naga.jpg" alt="Image">
			</div>
			<div class="clear"></div>	
		</div>

		<div class="div_team">
			<span class="site_general"><b>OUR TEAM</b></span>
			<p class="latin">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere c. Etiam ut dui eu nisi lobortis rhoncus 
							 ac quis nunc.</p>
			<div class="team_container">
				<div class="div_people">
					<img src="/web/img/boy.jpg" alt="Kevin Perry pic.">
					<span class="name"><b>KEVIN PERRY</b></span>
					<p class="developer">SOFTWARE DEVELOPER</p>
					<p class="text_about">Vivamus congue justo eget diam interdum scelerisque. In hac habitasse platea dictumst.</p>
					<a target="blank" href="https://www.instagram.com/" title="Instagram.com"><i class="fa fa-instagram"></i></a>
					<a target="blank" href="https://www.facebook.com/" title="Facebook.com"><i class="fa fa-facebook"></i></a>
					<a target="blank" href="https://plus.google.com/" title="Google-Plus.com"><i class="fa fa-google-plus"></i></a>
					<a target="blank" href="../../.." title="Email Us"><i class="fa fa-envelope"></i></a>
				</div>

				<div class="div_people">
					<img src="/web/img/recycle.jpg" alt="Sexy palomita pic.">
					<span class="name"><b>SEXY PALOMITA</b></span>
					<p class="developer">SOFTWARE DEVELOPER</p>
					<p class="text_about">Vivamus congue justo eget diam interdum scelerisque. In hac habitasse platea dictumst.</p>
					<a target="blank" href="https://www.instagram.com/" title="Instagram.com"><i class="fa fa-instagram"></i></a>
					<a target="blank" href="https://www.facebook.com/" title="Facebook.com"><i class="fa fa-facebook"></i></a>
					<a target="blank" href="https://plus.google.com/" title="Google-Plus.com"><i class="fa fa-google-plus"></i></a>
					<a target="blank" href="../../.." title="Email Us"><i class="fa fa-envelope"></i></a>
				</div>

				<div class="div_people">
					<img src="/web/img/haha.jpg" alt="Scare you pic.">
					<span class="name"><b>SCARE YOU</b></span>
					<p class="developer">SOFTWARE DEVELOPER</p>
					<p class="text_about">Vivamus congue justo eget diam interdum scelerisque. In hac habitasse platea dictumst.</p>
					<a target="blank" href="https://www.instagram.com/" title="Instagram.com"><i class="fa fa-instagram"></i></a>
					<a target="blank" href="https://www.facebook.com/" title="Facebook.com"><i class="fa fa-facebook"></i></a>
					<a target="blank" href="https://plus.google.com/" title="Google-Plus.com"><i class="fa fa-google-plus"></i></a>
					<a target="blank" href="../../.." title="Email Us"><i class="fa fa-envelope"></i></a>
				</div>

				<div class="div_people">
					<img src="/web/img/gurl.jpg" alt="Grand madamma pic.">
					<span class="name"><b>GRAND MADAMMA</b></span>
					<p class="developer">SOFTWARE DEVELOPER</p>
					<p class="text_about">Vivamus congue justo eget diam interdum scelerisque. In hac habitasse platea dictumst.</p>
					<a target="blank" href="https://www.instagram.com/" title="Instagram.com"><i class="fa fa-instagram"></i></a>
					<a target="blank" href="https://www.facebook.com/" title="Facebook.com"><i class="fa fa-facebook"></i></a>
					<a target="blank" href="https://plus.google.com/" title="Google-Plus.com"><i class="fa fa-google-plus"></i></a>
					<a target="blank" href="../../.." title="Email Us"><i class="fa fa-envelope"></i></a>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="general_information">

			<span class="site_general"><b>ABOUT</b></span>
			<p class="latin">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere c. Etiam ut dui eu nisi lobortis rhoncus 
							 ac quis nunc.</p>

			<div class="container_information">
				<div class="container_biography">
					<p class="text_information"><b>Biography</b></p>
					<p>Morbi rutrum, elit ac fermentum egestas, tortor ante vestibulum est, eget scelerisque nisl velit eget tellus. Fusce porta facilisis luctus. Integer neque dolor, rhoncus nec eiusmod eget, pharetra et tortor. Nulla id pulvinar nunc. Vestibulum auctor nisl vel lectus ullamcorper sed pellentesque dolor eleifend. Praesent lobortis magna vel diam mattis sagittis. Mauris porta odio eu risus scelerisque id facilisis ipsum dictum vitae volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar neque eu purus sollicitudin dui ultricies. Maecenas cursus auctor tellus sit amet blandit. Maecenas a erat ac nibh molestie interdum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed lorem enim, ultricies sed sodales id, convallis molestie ipsum. Morbi eget dolor ligula. Vivamus accumsan rutrum nisi nec elementum. Pellentesque at nunc risus. Phasellus ullamcorper bibendum varius. Quisque quis ligula sit amet felis ornare porta. Aenean viverra lacus et mi elementum mollis. Praesent eu justo elit.</p>
				</div>

				<div class="container_info">
					<p class="text_information"><b>Personal Info</b></p>
					<p><a target="blank" href="../../.." title="Call us"><i class="fa fa-phone"></i> <b>+1123 2456 689</b></a></p>
					<p><a target="blank" href="../../.." title="Here we are"><i class="fa fa-map-marker"></i> <b>3301 Lorem Ipsum, Dolor sit St.</b></a></p>
					<p class="admin"><a target="blank" href="../../.." title="Your email address"><i class="fa fa-envelope"></i> <b>admin@domain.com</b></a></p>
					<p class="admin"><a target="blank" href="../../.." title="Mail us"><i class="fa fa-internet-explorer"></i> <b>Mageewp.com</b></a></p>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="banner">
			<div class="banner_container">
				<div class="mini_banner">
					<p class="score">20</p>
					<p>THEMES</p>
				</div>

				<div class="mini_banner">
					<p class="score">98</p>
					<p>SUPPORTERS</p>
				</div>

				<div class="mini_banner">
					<p class="score">1360</p>
					<p>PROJECTS</p>
				</div>

				<div class="mini_banner">
					<p class="score">8000</p>
					<p>CUSTOMERS</p>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="div_story">
			<div class="story_container">
				<div class="story">
					<div class="quote">
						<i class="fa fa-quote-left"></i>
					</div>
					<div class="story_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consequat non ex quis consectetur. Aliquam iaculis dolor erat, ut ornare dui voluptate nec. Cras a sem mattis, tincidunt urna nec, iaculis nisl. Nam congue ultricies dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consequat non ex quis consectetur. Aliquam iaculis dolor erat, ut ornare dui voluptate nec. Cras a sem mattis, tincidunt urna nec, iaculis nisl. Nam congue ultricies dui.</p>
					</div>
					<div class="person">
						<div class="photo">
							<img src="/web/img/boy.jpg" alt="Kevin Perry image">
						</div>
						<div class="person_names">
							<p class="names">KEVIN PERRY</p>
							<p>Web Developer</p>
						</div>
					</div>
				</div>

				<div class="story">
					<div class="quote">
						<i class="fa fa-quote-left"></i>
					</div>
					<div class="story_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consequat non ex quis consectetur. Aliquam iaculis dolor erat, ut ornare dui voluptate nec. Cras a sem mattis, tincidunt urna nec, iaculis nisl. Nam congue ultricies dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consequat non ex quis consectetur. Aliquam iaculis dolor erat, ut ornare dui voluptate nec. Cras a sem mattis, tincidunt urna nec, iaculis nisl. Nam congue ultricies dui.</p>
					</div>
					<div class="person">
						<div class="photo">
							<img src="/web/img/recycle.jpg" alt="Sexy Palomita image">
						</div>
						<div class="person_names">
							<p class="names">SEXY PALOMITA</p>
							<p>Web Developer</p>
						</div>
					</div>
				</div>

				<div class="story">
					<div class="quote">
						<i class="fa fa-quote-left"></i>
					</div>
					<div class="story_text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consequat non ex quis consectetur. Aliquam iaculis dolor erat, ut ornare dui voluptate nec. Cras a sem mattis, tincidunt urna nec, iaculis nisl. Nam congue ultricies dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consequat non ex quis consectetur. Aliquam iaculis dolor erat, ut ornare dui voluptate nec. Cras a sem mattis, tincidunt urna nec, iaculis nisl. Nam congue ultricies dui.</p>
					</div>
					<div class="person">
						<div class="photo">
							<img src="/web/img/gurl.jpg" alt="Grand Madamma image">
						</div>
						<div class="person_names">
							<p class="names">GRAND MADAMMA</p>
							<p>Web Developer</p>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="div_contact">
			<span class="site_general"><b>CONTACT</b></span>
			<p class="latin">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere c. Etiam ut dui eu nisi lobortis rhoncus 
							 ac quis nunc.</p>
			<div class="div_form">
				<form>
					<input type="text" name="client_name" placeholder="Name">
					<input type="text" name="email_address" placeholder="Email">
					<textarea name="message" placeholder="Message"></textarea>
					<input type="button" class="button_post" name="post" value="Post">
				</form>
			</div>
			<div class="clear"></div>
		</div>

		<div class="div_footer">
			<div class="footer_container">
				<span><b><font color="white">Designed by</font> <font color="grey">MageeWP Themes.</font></b></span>
				<div class="div_links_footer">
					<a target="blank" href="https://www.facebook.com/" title="facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a target="blank" href="https://twitter.com/" title="twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a target="blank" href="https://www.linkedin.com/" title="linkedin.com"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					<a target="blank" href="https://www.youtube.com/" title="youtube.com"><i class="fa fa-youtube"></i></a>
					<a target="blank" href="https://www.skype.com/en/" title="skype.com"><i class="fa fa-skype" aria-hidden="true"></i></a>
					<a target="blank" href="https://ro.pinterest.com/" title="pinterest.com"><i class="fa fa-pinterest"></i></a>
					<a target="blank" href="https://plus.google.com/" title="google-plus.com"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
					<a target="blank" href="../../.." title="Your email address"><i class="fa fa-envelope"></i></a>
					<a target="blank" href="https://feed.com/#" title="feed.com"><i class="fa fa-rss" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	
	</body>

</html>